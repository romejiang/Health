Ext.define('Health.controller.MyInfor', {
    extend: 'Ext.app.Controller',
    requires: ['Ext.data.JsonP'],
    config: {
        routes:{
            loadMyInfor:'loadMyInfor'
        },
        refs: {
            main: 'mainPanel',
            home: "homePanel",
            register:'registerPanel',
            myinfor_confirm:'myInforPanel #myinfor_confirm'
        },
        control: {
            "backMainButton":{
                tap:'backMain'
            },
            'myinfor_confirm':{
                tap:function(){
                    var uid = localGet('app_Login_id');
                    var uname = localGet('app_Login_name');
                    var birth=Ext.getCmp('userbirth').getValue()
                    var birthyear=birth.getFullYear();
                    var birthmonth=birth.getMonth();
                    var birthday=birth.getDate();
                    Ext.Ajax.request({
                        url: Global.api_url + '/cloud/1/users_update',
                        params: {
                            cuid: uid,
                            name:Ext.getCmp('username').getValue(),
                            sex:Ext.getCmp('usersex').getValue(),
                            address:Ext.getCmp('useraddress').getValue(),
                            birthyear:birthyear,
                            birthmonth:birthmonth+1,
                            birthday:birthday
                        },
                        scope: this,
                        success: function (response) {
                            Ext.Viewport.unmask();
                            var result = Ext.decode(response.responseText);
                            cordova.exec(function (message) {
                            }, function (error) {
                            }, "Messages", "showMsg", [result.Variables.Message.messagestr]);
                            return false;
                        },
                        failure: function () {
                            Ext.Viewport.unmask();
                            cordova.exec(function (message) {
                            }, function (error) {
                            }, "Messages", "showMsg", ['请求失败', '服务器维护中....']);
                            return false;
                        }
                    })
                }
            }
        }
    }
});