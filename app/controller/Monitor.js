Ext.define('Health.controller.Monitor', {
    extend: 'Ext.app.Controller',

    requires: [
        'Ext.form.*',
        'Ext.field.*',
        'Ext.data.Store'
    ],
    config: {

        refs: {
            mainview: 'mainPanel',
            monitorPanel: 'monitorPanel',
            tnbFormPanel: 'tnbFormPanel',
            jsbFormPanel: 'jsbFormPanel',
            gxyFormPanel: 'gxyFormPanel',

            tnbListPanel: 'tnbListPanel',
            tnbFormBtnArea: '#tnbFormBtnArea',
            formCancelButton: 'button[action=formCancel]',
            tnbFormSubmitBtn: 'button[action=tnbFormSubmit]',

            gxyListPanel: 'gxyListPanel',
            gxyFormBtnArea: '#gxyFormBtnArea',
            gxyFormSubmitBtn: 'button[action=gxyFormSubmit]',
            gxyformCancel: 'button[action=gxyformCancel]',

            jsbListPanel: 'jsbListPanel',
            jsbFormBtnArea: '#jsbFormBtnArea',
            jsbFormSubmitBtn: 'button[action=jsbFormSubmit]',
            jsbformCancel: 'button[action=jsbformCancel]'

        },

        control: {
            "monitorPanel dataview": {
                itemtap: 'showmonitorForm'
            },

            "tnbListPanel dataview": {
                itemtap: 'showTnbForm'
            },
            "formCancelButton": {
                tap: 'formCancel'
            },
            "tnbFormSubmitBtn": {
                tap: function () {
                    Ext.Viewport.mask({
                        xtype: 'loadmask',
                        message: '添加中...'
                    });
                    var backthis = this;
                    Ext.Ajax.request({
                        url: Global.api_url + '/cloud/1/diabetes_add',
                        scope: this,
                        method: 'POST',
                        params: {
                            symptom: Ext.getCmp('symptom').getChecked() ? 1 : 0,
                            drinking: Ext.getCmp('drinking').getChecked() ? 1 : 0,
                            eat: Ext.getCmp('eat').getChecked() ? 1 : 0,
                            urine: Ext.getCmp('urine').getChecked() ? 1 : 0,
                            vision: Ext.getCmp('vision').getChecked() ? 1 : 0,
                            infect: Ext.getCmp('infect').getChecked() ? 1 : 0,
                            numb: Ext.getCmp('numb').getChecked() ? 1 : 0,
                            edema: Ext.getCmp('edema').getChecked() ? 1 : 0,
                            weightloss: Ext.getCmp('weightloss').getChecked() ? 1 : 0,
                            weight: Ext.getCmp('weight').getValue(),
                            smoke: Ext.getCmp('smoke').getValue(),
                            drink: Ext.getCmp('drink').getValue(),
                            uid: localGet('app_Login_id'),
                            cadid: localGet('cadid')
                        },
                        success: function (response) {
                            Ext.Viewport.unmask();

                            var result = Ext.decode(response.responseText)
                            if (result.Variables.Result.code == 0) {
                                var store = Ext.getStore('Tnb');
                                store.load({params: {
                                    uid: localGet('app_Login_id'),
                                    cadid: localGet('cadid')
                                },
                                    callback: function (data) {
                                        var record = {id: "add", symptom: "", drinking: "", eat: "", urine: "", vision: "", infect: "", numb: "", weightloss: "", weight: "", smoke: "", drink: "", uid: '', cadid: "", lasttime: ""};
                                        store.add(record);
                                        store.sync();
                                    }
                                })
                                gotoFn()
                            }

                        }
                    })
                }
            },

            "gxyListPanel dataview": {
                itemtap: 'showGxyForm'
            },
            "gxyFormSubmitBtn": {
                tap: function () {
                    Ext.Viewport.mask({
                        xtype: 'loadmask',
                        message: '添加中...'
                    });
                    var backthis = this;
                    Ext.Ajax.request({
                        url: Global.api_url + '/cloud/1/feritin_add',
                        scope: this,
                        method: 'POST',
                        params: {
                            symptom: Ext.getCmp('gxy_symptom').getChecked() ? 1 : 0,
                            dizzy: Ext.getCmp('gxy_dizzy').getChecked() ? 1 : 0,
                            tinnitus: Ext.getCmp('gxy_tinnitus').getChecked() ? 1 : 0,
                            dyspnea: Ext.getCmp('gxy_dyspnea').getChecked() ? 1 : 0,
                            xiongmen: Ext.getCmp('gxy_xiongmen').getChecked() ? 1 : 0,
                            bleed: Ext.getCmp('gxy_bleed').getChecked() ? 1 : 0,
                            numb: Ext.getCmp('gxy_numb').getChecked() ? 1 : 0,
                            edema: Ext.getCmp('gxy_edema').getChecked() ? 1 : 0,
                            weight: Ext.getCmp('gxy_weight').getValue(),
                            smoke: Ext.getCmp('gxy_smoke').getValue(),
                            drink: Ext.getCmp('gxy_drink').getValue(),
                            uid: localGet('app_Login_id'),
                            cadid: localGet('cadid')
                        },
                        success: function (response) {
                            Ext.Viewport.unmask();
                            var result = Ext.decode(response.responseText)
                            if (result.Variables.Result.code == 0) {
                                var store = Ext.getStore('Gxy');
                                store.load({params: {
                                    uid: localGet('app_Login_id'),
                                    cadid: localGet('cadid')
                                },
                                    callback: function (data) {
                                        var record = {id: "add", symptom: "", drinking: "", eat: "", urine: "", vision: "", infect: "", numb: "", weightloss: "", weight: "", smoke: "", drink: "", uid: '', cadid: "", lasttime: ""};
                                        store.add(record);
                                        store.sync();
                                    }
                                })
                                gotoFn()
                            }
                        }
                    })
                }
            },

            "jsbListPanel dataview": {
                itemtap: 'showJsbForm'
            },
            "jsbFormSubmitBtn": {
                tap: 'jsbFormSubmit'
            },
            "gxyformCancel": {
                tap: 'gxyformCancel'
            },
            "jsbformCancel": {
                tap: 'jsbformCancel'
            }
        }

    },

    //我的监测分类显示
    showmonitorForm: function (list, index, element, record) {
        if (record.get('id').indexOf('tnb') == 0) {
            var tnbStore = Ext.getStore('Tnb');
            var TnbList = this.getTnbListPanel();
            if (TnbList == null) {
                TnbList = Ext.create('Health.view.TnbList');
            }
            Ext.Viewport.animateActiveItem(TnbList, {type: 'slide', direction: 'left'});
        }
        if (record.get('id').indexOf('gxy') == 0) {
            var gxyStore = Ext.getStore('Gxy');
            var GxyList = this.getGxyListPanel();
            if (GxyList == null) {
                GxyList = Ext.create('Health.view.GxyList');
            }
            Ext.Viewport.animateActiveItem(GxyList, {type: 'slide', direction: 'left'});
        }
        if (record.get('id').indexOf('jsb') == 0) {
            var jsbStore = Ext.getStore('Jsb');
            var JsbList = this.getJsbListPanel();
            if (JsbList == null) {
                JsbList = Ext.create('Health.view.JsbList');
            }
            Ext.Viewport.animateActiveItem(JsbList, {type: 'slide', direction: 'left'});
        }
    },

    //清空表格数据
    formCancel: function () {
        this.getTnbFormPanel().reset();
    },

    gxyformCancel: function () {
        this.getGxyFormPanel().reset();
    },
    jsbformCancel: function () {
        this.getJsbFormPanel().reset();
    },

    //糖尿病新建和历史数据显示
    showTnbForm: function (list, index, element, record) {
        var tnbForm = this.getTnbFormPanel()
        if (tnbForm == null) {
            tnbForm = Ext.create('Health.view.TnbForm');
        }
        Ext.Viewport.animateActiveItem(tnbForm, {type: 'slide', direction: 'left'});
        if (record.getData().id != 'add') {
            tnbForm.setRecord(record);
            Ext.getCmp('tnbbutton').setHidden(true);
        } else {
            Ext.getCmp('tnbbutton').setHidden(false);
        }
    },

    //高血压新建和历史数据显示
    showGxyForm: function (list, index, element, record) {
        //id==add添加新的表单
        var gxyForm = this.getGxyFormPanel()
        if (gxyForm == null) {
            gxyForm = Ext.create('Health.view.GxyForm');
        }
        Ext.Viewport.animateActiveItem(gxyForm, {type: 'slide', direction: 'left'});
        if (record.getData().id != 'add') {
            gxyForm.setRecord(record);
            Ext.getCmp('gxybutton').setHidden(true);
        } else {
            Ext.getCmp('gxybutton').setHidden(false);
        }
    },

    //精神病新建和历史数据显示
    showJsbForm: function (list, index, element, record) {
        //id==add添加新的表单
        var jsbForm = this.getJsbFormPanel()
        if (jsbForm == null) {
            jsbForm = Ext.create('Health.view.JsbForm');
        }
        Ext.Viewport.animateActiveItem(jsbForm, {type: 'slide', direction: 'left'});
        if (record.get('id').indexOf('add') != 0) {
            jsbForm.setRecord(record);
        }
    },


    //精神病表单提交
    jsbFormSubmit: function () {
        var date = new Date();
        var today = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
        var jsbStore = Ext.getStore('Jsb');
        jsbStore.add({
            date: today,
            name: this.getName().getValue(),
            age: this.getAge().getValue(),
            jibing: this.getJibing().getValue()
        });
        jsbStore.sync();
        Ext.Viewport.animateActiveItem('jsbListPanel', {type: 'slide', direction: 'left'});
    },

    executeSuccess: function (result) {
        if (result.Variables.Result.code == 0) {
            Ext.getStore('AreaAllFindStore').load({params: {caid: localGet('caid')}});
            this.getArea().pop();
        } else {
            Ext.Msg.alert(result.Variables.Message.messagestr);
        }
    }

})