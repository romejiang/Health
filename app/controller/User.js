Ext.define('Health.controller.User', {
    extend: 'Ext.app.Controller',
    requires: ['Ext.data.JsonP'],
    config: {
        routes:{
            loadMyInfor:'loadMyInfor'
        },
        refs: {
            main: 'mainPanel',
            home: "homePanel",
            register:'registerPanel',
            myInfor:'myInforPanel',
            backMainButton:'button[action=backMain]',
            username:'myInforPanel #username',
            sex:'myInforPanel #sex',
            birth:'myInforPanel #birth',
            mobile_num:'myInforPanel #mobile_num',
            address:'myInforPanel #address'
        },
        control: {
            "backMainButton":{
                tap:'backMain'
            }
        }
    },

    loadMyInfor:function(){

        Ext.Ajax.request({
            url: Global.api_url + '/cloud/1/users_info_get',
            params: {cuid: window.localGet('app_Login_id')},
            scope: this,
            success: function (response) {
                var result = Ext.decode(response.responseText);
                var user = result.Variables.data;
                this.getUsername().setValue(user.rename);
                this.getSex().setValue(user.sex);
                console.log(user.birthyear)
                if(user.birthyear!=0)
                    this.getBirth().setValue(user.birthyear+'-'+user.birthmonth);
                this.getMobile_num().setValue(user.mobile_num);
                this.getAddress().setValue(user.address);
            },
            failure: function () {
                Ext.Viewport.unmask();
                cordova.exec(function (message) {
                }, function (error) {
                }, "Messages", "showMsg", ['请求失败', '服务器维护中....']);
                return false;
            }
        })
    }











});