Ext.define('Health.controller.FindPassword', {
    extend: 'Ext.app.Controller',

    valid_num:'',
    cuid: '',
    config: {
        refs: {
            findPassword:'findPassword',
            modify_password_button:'findPassword #modify_password_button',
            valid_button:'findPassword #valid_button',
            get_valid_button:'findPassword #get_valid_button'
        },
        control: {
            get_valid_button:{
                tap:function(){
                    Ext.Viewport.mask({xtype: 'loadmask',message: '获取验证码...'});
                    var user_tel=Ext.getCmp('user_tel').getValue();
                    if (Ext.isEmpty(user_tel)){
                        Ext.Viewport.unmask();
                        cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", ['请输入手机号码！']);
                        return false;
                    }
                    var backtis=this;
                    Ext.Ajax.request({
                        url: Global.api_url + '/cloud/1/send_message',
                        scope: this,
                        method: 'POST',
                        params: {
                            caid: Global.app_id,
                            mobiles: user_tel
                        },
                        success: function (response) {
                            Ext.Viewport.unmask();
                            var result=Ext.decode(response.responseText);
                            if (result.Variables.Result.code == 0) {
                                backtis.cuid = result.Variables.data.cuid;
                                backtis.valid_num = result.Variables.data.code;
                                Ext.getCmp('get_valid').setHidden(true);
                                Ext.getCmp('valid').setHidden(false);
                                cordova.exec(function (message) {
                                }, function (error) {
                                }, "Messages", "showMsg", ['验证码已发送']);
                            }else{
                                cordova.exec(function (message) {
                                }, function (error) {
                                }, "Messages", "showMsg", [result.Variables.Message.messagestr]);
                            }
                        },
                        failure: function () {//请求失败时执行操作
                            Ext.Viewport.unmask();
                            cordova.exec(function (message) {
                            }, function (error) {
                            }, "Messages", "showMsg", ['获取失败,请重试...']);
                        }
                    });
                }
            },
            valid_button:{
                tap:function(){
                    Ext.Viewport.mask({xtype: 'loadmask',message: '获取验证码...'});
                    var valid=Ext.getCmp('valid_num').getValue().toUpperCase();
                    if (Ext.isEmpty(user_tel)){
                        Ext.Viewport.unmask();
                        cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", ['请输入验证码！']);
                        return false;
                    }
                    if(valid ==this.valid_num.toUpperCase()){
                        Ext.Viewport.unmask();
                        Ext.getCmp('modify_password').setHidden(false);
                        Ext.getCmp('valid').setHidden(true);
                    }else{
                        Ext.Viewport.unmask();
                        Ext.getCmp('valid_num').setValue('');
                        cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", ['验证码错误，请重输！']);
                        return false;
                    }
                }
            },
            modify_password_button:{
                tap:function(){
                    Ext.Viewport.mask({xtype: 'loadmask',message: '提交中...'});
                    var new_password=Ext.getCmp('new_password').getValue();
                    var re_password=Ext.getCmp('re_password').getValue();
                    if (Ext.isEmpty(new_password)){
                        Ext.Viewport.unmask();
                        cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", ['请输入新密码！']);
                        return false;
                    }
                    if (Ext.isEmpty(re_password)){
                        Ext.Viewport.unmask();
                        cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", ['请确认密码！']);
                        return false;
                    }
                    if(new_password!=re_password){
                        Ext.Viewport.unmask();
                        cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", ['两次密码不一致，请重述！']);
                        Ext.getCmp('new_password').setValue('');
                        Ext.getCmp('re_password').setValue('');
                        return false;
                    }
                    var user_cuid=this.cuid;
                    Ext.Ajax.request({
                        url: Global.api_url + '/cloud/1/users_repassword_update',
                        scope: this,
                        method: 'POST',
                        params: {
                            cuid:user_cuid,
                            password:new_password
                        },
                        success: function (response) {
                            var result=Ext.decode(response.responseText);
                            if (result.Variables.Result.code == 0) {
                                Ext.Viewport.unmask();
                                cordova.exec(function (message) {
                                }, function (error) {
                                }, "Messages", "showMsg", ['密码修改成功！']);
                                gotoFn();
                            }else{
                                Ext.Viewport.unmask();
                                cordova.exec(function (message) {
                                }, function (error) {
                                }, "Messages", "showMsg", [result.Variables.Message.messagestr]);
                            }
                        },
                        failure: function () {//请求失败时执行操作
                            Ext.Viewport.unmask();
                            cordova.exec(function (message) {
                            }, function (error) {
                            }, "Messages", "showMsg", ['服务器连接错误,请重试...']);
                        }
                    });
                }
            }
        }
    }
});