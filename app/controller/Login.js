Ext.define('Health.controller.Login', {
    extend: 'Ext.app.Controller',
    requires: ['Ext.data.JsonP'],
    config: {
//        routes:{
//            loadLogin:'loadLogin'
//        },
        refs: {
            mainview: 'mainPanel',
            home: "homePanel",
            login:'loginPanel',
            register:'registerPanel',
            loginCancle:'button[action=loginCancle]',
            registerButton:'button[action=showrRegister]',
            loginSubmit:'button[action=loginSubmit]',
            txt_tel:'loginPanel #telField',
            txt_password:'loginPanel #passwordlField',
            txt_caid:'loginPanel #hidden_caidlField',
            txt_areaId:'loginPanel #hidden_areaIdField',
            advice: 'advice'

        },
        control: {
            "loginCancle":{
                tap:'loginCancle'
            },
            "registerButton":{
                tap:'showrRegister'
            },
            "loginSubmit":{
                tap:'loginSubmit'
            }
        }
    },

    // 进入登陆页面
//    loadLogin:function(){
//        Ext.Viewport.remove(this.getLogin(),true)
//        Ext.Viewport.animateActiveItem({xtype:'loginPanel'}, {type: 'slide', direction: 'left'});
//    },

    //取消登录返回主页
    loginCancle:gotoFn,

    //进入注册
    showrRegister:function(){
        gotoFn("registerPanel");
        return false;
    },

    /*点击登录按钮，提交成功后返回到首页中*/
    loginSubmit: function () {
        this.getTxt_password().blur();
        Ext.Viewport.mask({
            xtype: 'loadmask',
            message: '登录中...'
        });
        Ext.Viewport.remove(this.getAdvice(), true);
        var mobileNum=this.getTxt_tel().getValue();

        var flag=Ext.getCmp('passwordlField').getHidden();
        if(!flag){
            passwordValue=Ext.getCmp('passwordlField').getValue();
        }else{
            passwordValue=Ext.getCmp('showpassword').getValue();
        }

        this.getTxt_caid().setValue(Global.app_id);

        if (mobileNum == '') {
            cordova.exec(function (message) {
            }, function (error) {
            }, "Messages", "showMsg", ['手机不能为空！']);
            Ext.Viewport.unmask();
            return false;
        }
        if (passwordValue == '') {
            cordova.exec(function (message) {
            }, function (error) {
            }, "Messages", "showMsg", ['密码不能为空！']);
            Ext.Viewport.unmask();
            return false;
        }

        Ext.Ajax.request({
            url: Global.api_url + '/cloud/1/users_mobile_login',
            method: 'POST',
            params: {
                mobile_num: mobileNum,
                password:passwordValue,
                caid:Global.app_id
            },
            scope:this,
            success: function (response) {
                Ext.Viewport.unmask();
                var result = Ext.decode(response.responseText);
                cordova.exec(function (message) {
                }, function (error) {
                }, "Messages", "showMsg", [result.Variables.Message.messagestr]);
                if (result.Variables.Result.code == 0) {
                    localSave('app_Login_id', result.Variables.member_uid);//设置
                    localSave('app_Login_status', result.Variables.member_userstatus);
                    localSave('app_Login_name', result.Variables.member_username);
                    localSave('app_Login_avatar', result.Variables.avatar);
                    var user_cadid=result.Variables.data.cadid;
                    //社区Id为null,或者于当前社区ID不一致,更新社区ID
                    if(user_cadid==null || user_cadid=="" || localGet("cadid")!=user_cadid){
                        Health.app.getController("Area").updateUserForAreaFn(result.Variables.member_uid,localGet("cadid"))
                    }
                    //将登陆用户提交给cordova中
                    cordova.exec(function(winParam) {},function(error) {}, "GetApplicationInfo", "getApplicationInfo", [result.Variables.member_uid]);
//                    this.redirectTo('loadMain');
                    gotoFn();
                }
                else {
                    return false;
                }
            },
            failure: function () {
                Ext.Viewport.unmask();
                //请求失败时执行操作
                cordova.exec(function (message) {
                }, function (error) {
                }, "Messages", "showMsg", ['请求失败', '服务器维护中....']);
            }

        });
    }

});

