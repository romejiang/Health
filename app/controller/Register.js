Ext.define('Health.controller.Register', {
    extend: 'Ext.app.Controller',
    requires: ['Ext.data.JsonP'],
    config: {
//        routes:{
//            loadRegister:'loadRegister'
//        },
        refs: {
            login:'loginPanel',
            register:'registerPanel',
            registerCancleBtn:'button[action=registerCancle]',
            registerUserBtn:'button[action=registerUser]',
            txt_tel:'registerPanel #tel',
            txt_password:'registerPanel #password',
            txt_name:'registerPanel #name',
            txt_rePassword:'registerPanel #repassword',
            txt_caid:'registerPanel #hidden_caid',
            advice: 'advice'

        },
        control: {
            "registerCancleBtn":{
                tap:'registerCancle'
            },
            "registerUserBtn":{
                tap:'registerUser'
            }
        }
    },
    registerUser: function () {
        this.getTxt_password().blur();
        Ext.Viewport.mask({
            xtype: 'loadmask',
            message: '注册中...'
        });
        Ext.Viewport.remove(this.getAdvice(), true);
        var mobileNum=this.getTxt_tel().getValue();
        var passwordValue=this.getTxt_password().getValue();
        var nameValue=this.getTxt_name().getValue();
        this.getTxt_caid().setValue(Global.app_id);
        this.getTxt_rePassword().setValue(passwordValue);

        if (nameValue == '') {
            Ext.Viewport.unmask();
            cordova.exec(function (message) {
            }, function (error) {
            }, "Messages", "showMsg", ['姓名不能为空！']);
            return;
        }
        if (!this.verifyPhoneNumFn(mobileNum)) {
            Ext.Viewport.unmask();
            cordova.exec(function (message) {
            }, function (error) {
            }, "Messages", "showMsg", ['电话号码格式不正确！']);
            return;
        }
        if ( passwordValue == '' ) {
            Ext.Viewport.unmask();
            cordova.exec(function (message) {
            }, function (error) {
            }, "Messages", "showMsg", ['密码不能为空！']);
            return;
        }

        Ext.Ajax.request({
            url: Global.api_url + '/cloud/1/users_register',
            method: 'POST',
            params: {
                mobile_num: mobileNum,
                password:passwordValue,
                name:nameValue,
                repassword:passwordValue,
                caid:Global.app_id,
                cadid:localGet("cadid")
            },
            scope:this,
            success: function (response) {
                Ext.Viewport.unmask();
                var result = Ext.decode(response.responseText);
                if (result.Variables.Result.code == 0) {
                    localSave('app_Login_id', result.Variables.member_uid);//设置
                    localSave('app_Login_status', result.Variables.member_userstatus);
                    localSave('app_Login_name', result.Variables.member_username);
                    localSave('app_Login_avatar', result.Variables.avatar);
                    gotoFn("mainPanel");
                }
                cordova.exec(function (message) {
                }, function (error) {
                }, "Messages", "showMsg", [result.Variables.Message.messagestr]);
            },
            failure: function () {//请求失败时执行操作
                Ext.Viewport.unmask();
                cordova.exec(function (message) {
                }, function (error) {
                }, "Messages", "showMsg", ['请求失败', '服务器维护中....']);
                return false;
            }
        });
    },
    //验证手机号码格式
    verifyPhoneNumFn:function(num){
        var regex=/^0?(13[0-9]|15[012356789]|18[0-9]|14[57])[0-9]{8}$/;
        return regex.test(num);
    }
});