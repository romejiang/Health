Ext.define('Health.controller.Area', {
    extend: 'Ext.app.Controller',
    requires: ['Ext.data.JsonP'],
    config: {
        refs: {
            main: 'mainPanel',
            home: "homePanel",
            account: "account",
            index: 'index',
            area: 'areaPanel',
            searchArea: 'areaPanel #searchArea',
            searchbar: 'areaPanel #searchbar',
            areaButton: 'button[action=showArea]'
        },
        control: {
            "areaPanel list": {
                itemtap: function (list, index, element, record) {
                    localSave('app_name', record.get('name'));//设置
                    localSave('cadid', record.get('cadid'));//设置
                    localSave('address', record.get('address'));//设置
                    //用户社区绑定
                    if(!Ext.isEmpty(localGet("app_Login_id"))){
                        this.updateUserForAreaFn(localGet("app_Login_id"),record.get("cadid"));
                    }
                    var mainView = this.getMain();
                    if (mainView == null) {
                        mainView = Ext.create("Health.view.Main");
                    }
                    Ext.getStore('Home').load({params: {cadid: localGet('cadid'), udid: localGet('udid')} });
                    Ext.Viewport.remove(this.getArea(), true);
                    Ext.Viewport.remove(this.getAccount(), true);
                    mainView.setActiveItem("index");
                    gotoFn(mainView);
                }
            },
            searchArea: {
                change: function () {
                    var areaStore = Ext.getStore('Area');
                    var value = this.getSearchArea().getValue();
                    areaStore.clearFilter();
                    if (value != "") {
                        areaStore.filter('name', value, true);
                    }
                    areaStore.load();
                }
            }
        }
    },
    /**
     * 更新用户所属社区
     * @param uid 用户id
     * @param areaid  所属社区的Id
     */
    updateUserForAreaFn:function(uid,areaid){ 
        var areaObj={
            cadid:areaid,
            cuid:uid
        } 
        Ext.Ajax.request({
            url:Global.api_url+"/cloud/1/users_update_district",
            params:areaObj,
            scope:this,
            method:"POST",
            success:function(response){
                console.log("进入更新方法...,打印更新URL");
                console.log(Global.api_url+"/cloud/1/users_update_district?"+"cadid="+areaid+"&cuid="+uid);
                var result = Ext.decode(response.responseText);
                cordova.exec(function (message) {
                }, function (error) {
                }, "Messages", "showMsg", [result.Variables.Message.messagestr]);
            },failure:function(){
                //请求失败时执行操作
                cordova.exec(function (message) {
                }, function (error) {
                }, "Messages", "showMsg", ['请求失败', '服务器维护中....']);
            }
        });
    }
});