Ext.define('Health.controller.Main', {
    extend: 'Ext.app.Controller',
    requires: ['Ext.data.JsonP'],
    order_id: '',
    order_name: '',
    vaccine_name: '',
    vaccine_message: '',
    putOffDate: '',
    nextTime: '',
    config: {
        routes: {
            loadMain: 'loadMain',
            loadnotice:'loadNotice'
        },
        refs: {
            mainview: 'mainPanel',
            home: "homePanel",
            inforList: 'inforList',
            inforDetail: 'inforDetail',
            immunity: 'immunity',
            planimmune: 'planimmune',
            planList: "planlist",
            shooted: 'planimmune #shooted',
            putOffDate_done: '#putOffDate_done',
            orderDate: 'orderDate',
            orderForm: 'orderForm',
            popmap: 'popmap',
            login: 'loginPanel',
            area: 'areaPanel',
            medicare: 'medicare',
            medicareField: 'medicare #medicareField',
            medicareAdd: 'button[action=medicare]',
            areaButton: 'button[action=showArea]',
            loginButton: 'button[action=showLongin]',
            loginOutButton: 'button[action=loginOut]',
            immunitySearch: 'button[action=immunitySearch]',
            planSearch: "button[action=planSearch]",
            orderSubmit: 'orderForm button[action=orderSubmit]',
            feUseDetail: 'setplanview panel[cls=feUseDetail]'
        },
        control: {
            mainview: {
                activeitemchange: function (tabPanel, tab, oldTab, events) {
                    var mainview = this.getMainview();
                    if (tab.config.xtype == 'monitorPanel') {
                        if (window.localGet('app_Login_id') == null || window.localGet('app_Login_name') == null) {
                            Ext.Viewport.animateActiveItem('loginPanel', {type: 'slide', direction: 'left'});
                            return false;
                        }
                    }
                }

            },
            "homePanel dataview": {
                itemtap: function (list, index, element, record) {
                    var inforList = this.getInforList();
                    var ctype = record.get("ctype");

                    //地图
                    if (ctype == 5) {
                        var popmap = Ext.create('Health.view.PopMap');
                        Ext.Viewport.animateActiveItem(popmap, {type: 'slide', direction: 'left'});
                        popmap.search(localGet('address'));
                    }
                    //计划免疫
                    else if (ctype == 2) {
                        var immunity = Ext.create('Health.view.Immunity')
                        Ext.Viewport.animateActiveItem(immunity, {type: 'slide', direction: 'left'});
                        Ext.Ajax.request({
                            url: Global.api_url + '/cloud/1/article_info_get',
                            method: 'POST',
                            params: {caid: 113},
                            scope: this,
                            success: function (response) {
                                Ext.Viewport.unmask();
                                var result = Ext.decode(response.responseText);
                                if (result.Variables.Result.code == 0) {
                                    if (result.Variables.data != '') {
                                        Ext.getCmp('immunity_detail').setHtml(
                                            '<div class="immunity_title">' + result.Variables.data.name + '</div>' +
                                                '<div class="immunity_content">' + result.Variables.data.content + '</div>'
                                        )
                                    }
                                }
                            }
                        });
                        var d = new Date();
                        var resultDate = null; //上次接种时间
                        if (d.getDate() * 1 > 15) {
//                            Ext.getCmp("last_date").setValue(new Date(d.getFullYear() + "-" + (d.getMonth() - 1) + "-15"));
                        } else {
                            var d1 = new Date(this.getPreDate(1));//获取上一个月
                            //   console.log(d1.getMonth());
//                            Ext.getCmp("last_date").setValue(new Date(d1.getFullYear() + "-" + (d1.getMonth() + 1) + "-15"));
                            //console.log(d1.getFullYear()+"-"+ (d1.getMonth()+1)+"-"+"15");
                        }
                    }
                    //居民医保
                    else if (ctype == 4) {
                        var medicare = Ext.create('Health.view.Medicare')
                        Ext.Viewport.animateActiveItem(medicare, {type: 'slide', direction: 'left'});
                        Ext.Ajax.request({
                            url: Global.api_url + '/cloud/1/article_info_get',
                            method: 'POST',
                            params: {caid: 101},
                            scope: this,
                            success: function (response) {
                                Ext.Viewport.unmask();
                                var result = Ext.decode(response.responseText);
                                if (result.Variables.Result.code == 0) {
                                    if (result.Variables.data != '') {
                                        Ext.getCmp('medicare_detail').setHtml(
                                            '<div class="immunity_title">' + result.Variables.data.name + '</div>' +
                                                '<div class="immunity_content">' + result.Variables.data.content + '</div>'
                                        );
                                    }
                                }
                            }
                        });
                    }
                    //预约就诊
                    else if (ctype == 3) {
                        var order = Ext.create('Health.view.Order');
                        //Ext.Viewport.animateActiveItem(order, {type: 'slide', direction: 'left'});
                        gotoFn(order);
                    }
                    //120
                    else if (ctype == 6) {
                        cordova.exec(function (successCallback) {
                        }, function (errorCallback) {
                        }, "Redirect", "callNumber", ['120']);
                        return false;
                    }
                    else if (ctype == 7 || ctype == 8) {
                        localSave("fectype", ctype);
                        Ext.Viewport.animateActiveItem({xtype: 'setplanview'}, {type: 'slide', direction: 'left'});
                        Ext.getCmp('planName').setTitle(record.get('name'));
                        Ext.getCmp("plan_start_date").setLabel(ctype == 7 ? "出生日期:" : "最后一次生理期:")
                        var eDetail = '<div class="immunity_title">使用说明</div>' +
                                '<div class="immunity_content">社区卫生中心儿童保健免费服务：<br/>　查体年龄：满月、三月、六月、八月、一岁、一岁半、两岁、两岁半、三岁。<br/>　输入儿童出生日期，设置到时提醒。</div>',
                            fDetail = '<div class="immunity_title">使用说明</div>' +
                                '<div class="immunity_content">社区卫生中心妇女保健免费服务：<br/>　五次产前检查：怀孕12周、16周、21周、25周、37周。<br/>　二次产后访视：产后7天、产后28天。<br/>　一次健康检查：产后42天。<br/>　输入最后一次生理期的第一天时间，计算预产期，设置产前检查提醒。</div>';
                        this.getFeUseDetail().setHtml(ctype == 7 ? eDetail : fDetail);
                        if(ctype==7){ //最后一次儿童设置初始值为上年7月15日
                           // var d=new Date();
                           // Ext.getCmp("plan_start_date").setValue(new Date((d.getFullYear() - 1) +'-7-15'));
                           Ext.getCmp("plan_start_date").setValue();
                        }
                        if(ctype==8){ //最后一次生理期时间设置初始值为今日前2个月的15日
                           // var d=new Date(this.getPreDate(2));
                           // Ext.getCmp("plan_start_date").setValue(new Date(d.getFullYear()+'-'+ (d.getMonth()+1)+'-15'));
                           Ext.getCmp("plan_start_date").setValue();

                        }
                    }
                    //最新通知
                    else if(ctype == 9)
                    {
                        Ext.Viewport.animateActiveItem('notice', {type: "slide", direction: "left"});
                    }
                    //资讯列表
                    else {
                        Ext.Viewport.animateActiveItem({xtype: 'inforList', params: {ccid: record.get('ccid'), udid: localGet('udid')}}, {type: 'slide', direction: 'left'});
                        Ext.getCmp('infor_title').setTitle(record.get('name'));
                    }
                }
            },
            "inforList list": {
                itemtap: function (list, index, element, record) {
                    if (record.get('ccid') == 64) {
                        if (record.get('notread') != 0) {
                            Ext.Ajax.request({
                                url: Global.api_url + '/cloud/1/article_read_add',
                                params: {
                                    caid: record.get('caid'),
                                    udid: localGet('udid')
                                },
                                scope: this,
                                method: 'POST',
                                success: function (result) {
                                    Ext.getStore('Infor').load({params: {ccid: record.get('ccid'), udid: localGet('udid')} });
                                    Ext.getStore('Home').load({params: {cadid: '10', udid: localGet('udid')} });
                                }
                            });
                        }
                    }
                    var inforDetail = this.getInforDetail();
                    if (inforDetail == null) {
                        inforDetail = Ext.create('Health.view.InforDetail');
                        var image = '';
                        if ((record.get('picurl') != '')) {
                            image = '<div style="text-align: center; margin-top: 5px;"><img src="' + record.get('picurl') + '" style=" width:94%; padding:5px;background:#fff;box-shadow:0px 0px 2px #aeb0b0; border:none;"  /></div>'
                        }
                        Ext.getCmp('detail').setHtml(
                            '<div style="background:#d6e2e2; border-bottom:1px solid #b3c8c7;text-align: center; padding: 10px;">' +
                                '<div style="font-size: 14px; font-weight: bold;color: #595858; width:100%; line-height: 30px">' + record.get('name') + '</div>' +
                                '<div style=" font-size: 12px; color: #b0b0b0; line-height: 24px;">' + [Ext.Date.format(new Date(parseInt(record.get('lasttime')) * 1000), 'Y-m-d')] + '</div>' +
                                '</div>' +
                                image +
                                '<div style="padding:10px;font-size: 12px;color: #6c6b6b;line-height: 26px">' + record.get('content') + '</div>'
                        )
                        Ext.Viewport.animateActiveItem(inforDetail, {type: "slide", direction: "left"});
                    }
                }
            },
            "order list": {
                itemtap: function (list, index, element, record) {
                    var isorder = record.get('isorder_arr');
                    var store = Ext.getStore('OrderDate');
                    store.removeAll();
                    store.add(isorder);
                    store.sync();
                    this.order_id = record.get('cpid');
                    this.order_name = record.get('name');
                    var orderDate = this.getOrderDate();
                    if (orderDate == null) {
                        orderDate = Ext.create('Health.view.OrderDate');
                    }
                    Ext.Viewport.animateActiveItem(orderDate, {type: 'slide', direction: 'left'});
                }
            },
            'orderSubmit': {
                tap: function () {
                    var orderer = Ext.getCmp('orderer').getValue();
                    var picktype = Ext.getCmp('picktype').getValue();
                    var otel = Ext.getCmp('otel').getValue();

                    if (Ext.isEmpty(orderer)) {
                        cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", ['请输入预约人姓名！']);
                        return;
                    }
                    if (Ext.isEmpty(otel)) {
                        cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", ['预约人电话！']);
                        return;
                    }
                    var cpid = this.order_id,
                        pname = this.order_name;
                    Ext.Ajax.request({
                        url: Global.api_url + '/cloud/1/orders_add',
                        scope: this,
                        method: 'POST',
                        params: {
                            orderer: orderer,
                            picktype: picktype,
                            otel: otel,
                            lasttime: Ext.getCmp('lasttime').getValue(),
                            pname: pname,
                            cpid: cpid,
                            cadid: localGet('cadid')
                        },
                        success: this.executeSuccess
                    });
                }
            },

            "orderDate list": {
                itemtap: function (list, index, element, record, target, event) {
                    if (target.target.name == 'click_order') {
                        var orderForm = this.getOrderForm();
                        if (orderForm == null) {
                            orderForm = Ext.create('Health.view.OrderForm');
                        }
                        Ext.getCmp('lasttime').setValue(record.get('date'));
                        Ext.Viewport.animateActiveItem(orderForm, {type: 'slide', direction: 'left'});
                    }
                }
            },
            "areaButton": {
                tap: 'showArea'
            },
            "loginButton": {
                tap: 'showLongin'
            },
            "loginOutButton": {
                tap: 'loginOut'
            },
            //计划免疫查询
            'immunitySearch': {
                tap: function () {
                    var name = Ext.getCmp('immunity_name').getValue();
                    var birth = new Date(Ext.getCmp('immunity_birthday').getValue());
                    var last_date = new Date(Ext.getCmp('last_date').getValue());
                    if (Ext.isEmpty(name)) {
                        name = '宝宝';
                    }
                    if (birth > new Date()) {
                        cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", ['出生日期超过今天了！请重新选择']);
                        return;
                    }
                    if (last_date > new Date()) {
                        cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", ['上次注射时间超过今天了！请重新选择']);
                        return;
                    }
                    var backThis = this;
                    var lastID;

                    lastID = Ext.getCmp('last_vaccine_id').getValue();
                    Ext.Viewport.mask({xtype: 'loadmask'});
                    this.getImmunity().submit({
                        url: Global.api_url + '/cloud/1/immunity_date_next_get',
                        method: 'POST',
                        scope: this,
                        success: function (from, result) {
                            Ext.Viewport.unmask();
                            var html;

                            if (result.Variables.Result.code == 0) {
                                backThis.vaccine_name = result.Variables.data.name;
                                backThis.vaccine_message = result.Variables.data.message;
                                backThis.nextTime = new Date(result.Variables.data.times + " 00:00:00");
                                localSave('immunity_name', name);
                                localSave('immunity_birthday', birth);
                                localSave('last_vaccine_id', lastID);
                                localSave('last_date', Ext.getCmp('last_date').getValue());
                                Ext.Viewport.animateActiveItem('planimmune', {type: 'slide', direction: 'left'});
                                var isPutOff = localGet('isPutOff');
                                var shootBtn = Ext.getCmp('shooted');
                                if (!isPutOff || isPutOff == null) {
                                    //没有推迟过的疫苗显示信息和设置闹铃
                                    backThis.immunityDetail(result.Variables.data.name, result.Variables.data.message);
                                    backThis.setClock(result.Variables.data.name, result.Variables.data.times);
                                    Ext.getCmp('putOffDate').setValue(new Date(result.Variables.data.times));
                                    if (new Date(result.Variables.data.times) < new Date()) {
                                        shootBtn.setCls('shoot1');
                                    } else {
                                        shootBtn.setCls('shoot0');
                                    }
                                } else {
                                    //推迟过的闹铃不在设置闹铃，只显示推迟后的信息
                                    var times = localGet('putOffDate');
                                    times = new Date(times)
                                    var date = times.getFullYear() + '-' + (times.getMonth() + 1) + '-' + times.getDate()
                                    backThis.immunityDetail(result.Variables.data.name, result.Variables.data.message);
                                    Ext.getCmp('putOffDate').setValue(times);
                                    if (times < new Date()) {
                                        shootBtn.setCls('shoot1');
                                    } else {
                                        shootBtn.setCls('shoot0');
                                    }
                                }
                            }

                        }
                    });
                }
            },
            "planSearch": {
                tap: function () {
                    // console.log("dddd:"+Ext.getCmp('plan_start_date').getValue());
                    // return;
                    if(Ext.getCmp('plan_start_date').getValue()==null){
                         cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", ['请选择日期！']);
                        return;
                    }
                    var inputDate = new Date(Ext.getCmp('plan_start_date').getValue());
                    if (inputDate > new Date()) {
                        cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", ['日期超过今天了！请重新选择']);
                        return;
                    }
                    Ext.Viewport.mask({xtype: 'loadmask'});
                    //拼接actionName和参数
                    var paramStr = (localGet("fectype") == 7 ? "erbao_date_all_get?birthday_date=" : "fubao_date_all_get?last_menstrual_date=") + Ext.util.Format.date(Ext.getCmp("plan_start_date").getValue(), "Y-m-d") + "&key_value=" + localGet("keyValue");
                    //请求数据
                    this.requestPlanListFn(paramStr);


                }
            },

            "planlist dataview": {
                itemtap: function (list, index, element, record, target, event) {
                    console.log(record)
                    if (record.get("select") * 1 == 1) {
                        Ext.MessageBox.YESNO =
                            [
                                {text: '是', itemId: 'yes', ui: 'action'},
                                {text: '否', itemId: 'no', ui: 'action'}
                            ];

                        var that = this;
                        Ext.Msg.confirm('删除提醒?', '', function (data) {
                            if (data == 'yes') {
                                Ext.Viewport.mask({xtype: 'loadmask'});
                                localSave("keyValue", record.get("key_value"))
                                //拼接actionName和参数
                                var paramStr = (localGet("fectype") == 7 ? "erbao_date_all_get?birthday_date=" : "fubao_date_all_get?last_menstrual_date=") + Ext.util.Format.date(Ext.getCmp("plan_start_date").getValue(), "Y-m-d") + "&key_value=" + localGet("keyValue");
                                //请求数据

                                that.requestPlanListFn(paramStr);
                                //获得并删除该月的所有已设置的闹铃
                                var clockArr = record.get("clockStr").split("-");
                                Ext.each(clockArr, function (items) {
                                    console.log("删除Id为" + items + "的闹铃")
                                    cordova.exec(function (winParam) {
                                    }, function (error) {
                                    }, "AlarmClock", "deleteManyAlarmClock", [
                                        [items]
                                    ]);
                                })
                                setTimeout(function () {
                                    Ext.Viewport.unmask();
                                }, 500)
                            }
                        });

                    }


                }

            },
            'shooted': {
                tap: function () {
                    var date = new Date();
                    var nextTime = new Date(this.nextTime);
                    console.log(nextTime)
                    if (date < nextTime) {
                        cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", ['' +
                            '还未到注射时间，无权操作']);
                        return;
                    }
                    var lastID = parseInt(localGet('last_vaccine_id')) + 1;
                    var backThis = this;
                    Ext.Viewport.mask({xtype: 'loadmask'});
                    Ext.Ajax.request({
                        url: Global.api_url + '/cloud/1/immunity_date_next_get',
                        method: 'POST',
                        scope: this,
                        params: {
                            last_date: date,
                            last_vaccine_id: lastID
                        },
                        success: function (result) {
                            Ext.Viewport.unmask();
                            localSave('isPutOff', false);
                            var html;
                            var result = Ext.decode(result.responseText);
                            if (result.Variables.Result.code == 0) {
                                backThis.vaccine_name = result.Variables.data.name;
                                backThis.vaccine_message = result.Variables.data.message;
                                backThis.nextTime = result.Variables.data.times;
                                backThis.immunityDetail(result.Variables.data.name, result.Variables.data.message);
                                backThis.setClock(result.Variables.data.name, result.Variables.data.times);
                                Ext.getCmp('putOffDate').setValue(new Date(result.Variables.data.times));
                                var shootBtn = Ext.getCmp('shooted');
                                if (new Date(result.Variables.data.times) < new Date()) {
                                    shootBtn.setCls('shoot1');
                                } else {
                                    shootBtn.setCls('shoot0');
                                }
                            }
                            localSave('last_vaccine_id', lastID);
                            localSave('last_date', date);
//                            Ext.getCmp('immunityDetail').setHtml(html)
                        },
                        failure: function () {
                            backThis.failed();
                        }

                    });
                }
            },
            'putOffDate_done': {
                tap: function () {
                    Ext.Viewport.mask({xtype: 'loadmask'});
                    var that = this;
                    setTimeout(function () {
                        var today = new Date();
                        localSave('isPutOff', true);
                        var value = new Date(Ext.getCmp('putOffDate').getValue());
                        var nextTime = new Date(that.nextTime);
                        if ((value.getTime() < nextTime.getTime())) {
                            cordova.exec(function (message) {
                            }, function (error) {
                            }, "Messages", "showMsg", ['调整时间应该晚于应注射时间！请重新选择']);
                            if (localGet('putOffDate') == null) {
                                Ext.getCmp('putOffDate').setValue(new Date(that.nextTime));
                            } else {
                                Ext.getCmp('putOffDate').setValue(new Date(localGet('putOffDate')));
                            }
                            Ext.Viewport.unmask();
                            return;
                        }
                        var maxTime = new Date(nextTime.setMonth(nextTime.getMonth() + 4));
                        if (value > maxTime) {
                            cordova.exec(function (message) {
                            }, function (error) {
                            }, "Messages", "showMsg", ['时间太久了啊！请重新选择']);
                            Ext.Viewport.unmask();
                            return;
                        }
                        var name = that.vaccine_name;
                        localSave('putOffDate', value);
                        var date = value.getFullYear() + '-' + (value.getMonth() + 1) + '-' + value.getDate();
                        cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", [ '已推迟到' + date + '之后注射疫苗！']);
                        that.setClock(name, value);

                        Ext.Viewport.unmask();

                    }, 100);


                }
            },

            //居民医保
            'medicareAdd': {
                tap: function () {
                    var medicare_name = Ext.getCmp('medicare_name').getValue();
                    var medicare_tel = Ext.getCmp('medicare_tel').getValue();
                    var medicare_num = Ext.getCmp('medicare_num').getValue();

                    if (Ext.isEmpty(medicare_name)) {
                        cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", ['请输姓名']);
                        return;
                    }
                    if (Ext.isEmpty(medicare_tel)) {
                        cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", ['请输电话']);
                        return;
                    }
                    if (Ext.isEmpty(medicare_num)) {
                        cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", ['请输社保号']);
                        return;
                    }
                    Ext.Viewport.mask({xtype: 'loadmask'});
                    Ext.Ajax.request({
                        url: Global.api_url + '/cloud/1/medicare_add',
                        params: {
                            name: medicare_name,
                            tel: medicare_tel,
                            number: medicare_num
                        },
                        scope: this,
                        method: 'POST',
                        success: function (result) {
                            var result = Ext.decode(result.responseText);
                            Ext.Msg.alert(result.Variables.Message.messagestr);
                            if (result.Variables.Result.code == 0) {
                                localSave('medicare_name', medicare_name)
                                localSave('medicare_tel', medicare_tel)
                                localSave('medicare_num', medicare_num)
                                Ext.getCmp('medicare_button').setHidden(true)
                                Ext.Viewport.unmask();
                            }
                        }
                    });
                }
            }
        }
    },
    loadNotice:function(){
        Ext.Viewport.animateActiveItem('notice', {type: "slide", direction: "left"});
    },
    failed: function () {
        Ext.Viewport.unmask();
        cordova.exec(function (message) {
        }, function (error) {
        }, "Messages", "showMsg", ['请求失败,服务器维护中....']);
    },
    //疫苗信息显示
    immunityDetail: function (name, message) {
        var html = "<div style='color: #959595; text-align: center'>" + name + "</div>";
        Ext.getCmp('immunityDetail').setHtml(html);
        Ext.getCmp('immunityIntroduce').setHtml(
            '<div class="immunityIntroduce">' + message +
                '</div>'
        );
    },

    //设置闹铃
    setClock: function (name, time) {
        var clock1;
        var clock2 = [];
        clock1 = new Date((new Date(time)).setDate(new Date(time).getDate() - 1));//前一天
        clock1 = new Date(new Date(clock1).setHours(20));//前一天晚八点
        this.clock(20, "明天该打'" + name + "'疫苗了", clock1);
        var date = new Date(new Date(clock1).setHours(8));//早上8点
        for (i = 0; i < 10; i++) {
            clock2[i] = new Date((new Date(date)).setDate(new Date(date).getDate() + 1));//推后一天
            this.clock(i, "今天该打'" + name + "'疫苗了", clock2[i]);//疫苗当天以及之后十天定十天闹铃
            date = clock2[i];
        }
    },
    /**
     * 闹铃
     * @param id 闹铃唯一标识
     * @param text  提示内容
     * @param time  时间
     */
    clock: function (id, text, time) {
        cordova.exec(function (winParam) {
            var clockTime = Math.round(new Date(time).getTime() / 1000);
            cordova.exec(
                function (winParam) {
                },
                function (error) {
                },
                "AlarmClock",
                "addAlarmClock",
                [id, text, clockTime.toString(), "100", "提醒"]
            );
        }, function (error) {
        }, "AlarmClock", "deleteManyAlarmClock", [
            [id]
        ]);

    },
    /**
     * 妇保,儿保计划列表展示
     * @param actionName  请求action的名称
     * @param paramObj  参数对象
     */
    requestPlanListFn: function (paramStr) {

        var planListItem = this.getPlanList() || Ext.create("Health.view.PlanList");
        var that = this;
        Ext.Viewport.animateActiveItem(planListItem, {type: 'slide', direction: 'left'});
        var dataStore = Ext.getCmp("planListDataView").getStore();
        var listDatas = new Array();
        Ext.Ajax.request({
            url: Global.api_url + '/cloud/1/' + paramStr,
            method: 'POST',
            scope: this,
            success: function (result) {
                if (dataStore != null) {
                    dataStore.removeAll();
                }
                var resultData = Ext.decode(result.responseText).Variables.data;

                var s = "2", clockId = 101;
                Ext.each(resultData, function (items) {
                    var clockArr = new Array();
                    Ext.each(items.date, function (itemsChild) {
                        if (itemsChild.isclock == 1) {
                            s = "1";
                            that.clock(clockId, items.message, new Date(itemsChild.daytime))
                            clockArr.push(clockId);
                            clockId++;
                        }
                    })
                    var dataObj = {
                        clockStr: clockArr.join("-"),
                        message: items.message,
                        select: s,
                        daymes: items.daymes || "",
                        key_value: items.key_value
                    }
                    listDatas.push(dataObj);
                })

                Ext.getCmp("planListDataView").setData(listDatas);
                Ext.Viewport.unmask();
            },
            failure: function () {
                this.failed();
            }

        });
    },


    //弹出地区选择列表
    showArea: function () {
        Ext.Viewport.remove(this.getArea(), true);
        Ext.Viewport.animateActiveItem({xtype: 'areaPanel'}, {type: 'slide', direction: 'left'});
    },

    // 进入登陆页面
    showLongin: function () {
        Ext.Viewport.remove(this.getLogin(), true)
        Ext.Viewport.animateActiveItem({xtype: 'loginPanel'}, {type: 'slide', direction: 'left'});
    },

    //退出登陆
    loginOut: function () {
        Ext.Msg.confirm('是否退出', '', function (data) {
            Ext.Viewport.mask({
                xtype: 'loadmask',
                message: '退出中...'
            });
            if (data == 'yes') {
                Ext.Ajax.request({
                    url: Global.api_url + '/cloud/1/users_logout',
                    method: 'POST',
                    scope: this,
                    success: function (response) {
                        Ext.Viewport.unmask();
                        var returnText = Ext.decode(response.responseText);

                        cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", [returnText.Variables.Message.messagestr]);
                        if (returnText.Variables.Result.code == 0) {
                            localRemove('app_Login_id');
                            localRemove('app_Login_status');
                            localRemove('app_Login_name');
                            localRemove('app_Login_avatar');
                            this.getLoginOutButton().setHidden(true);
                            this.getLoginButton().setHidden(false);

                        }
                    },
                    failure: function () {//请求失败时执行操作
                        Ext.Viewport.unmask();
                        cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", ['请求失败', '服务器维护中....']);
                        return false;
                    }
                })
            }
            else {
                Ext.Viewport.unmask();
            }
        }, this);
    },
    executeSuccess: function (result) {
        var result = Ext.decode(result.responseText);
        if (result.Variables.Result.code == 0) {
            var store = Ext.getStore('Order');
            store.load({params: {cadid: localGet('cadid')}})
            Ext.getCmp('orderfieldset').setHidden(true);
            Ext.getCmp('detailfieldset').setHidden(false);
            Ext.getCmp('order_detail').setHtml(
                '<div>预约号：' + result.Variables.data.ordersNo + '</div>' +
                    '<div>预约时间：' + Ext.getCmp('lasttime').getValue() + '</div>' +
                    '<div>预约医生：' + this.order_name + '</div>'
            )
        }
    },
    /**
     * 获取当前日期前若干个月的日期
     * @param num
     * @returns {*}
     */
    getPreDate: function (num) {
        var d1 = new Date();
        var titdate = Ext.util.Format.date(new Date(d1.getFullYear(), d1.getMonth() - num, d1.getDate()), "Y-m-d");
        return titdate;

    }

});