Ext.define('Health.controller.Advice', {
    extend: 'Ext.app.Controller',
    requires: ['Ext.data.JsonP'],
    config: {

        refs: {
            mainview: 'mainPanel',
            advice: 'advice',
            adviceCategory: 'adviceCategory',
            adviceDetail: 'adviceDetail',
            askPanel: 'askPanel',
            askButton: 'button[action=showAsk]',
            sendBtn: 'button[action=send]',
            asksubmit: 'button[action=asksubmit]',
            textsend: 'adviceDetail #textsend',
            askText: 'askPanel #askText',
//            cid: 'advice #cid',
            sendID: 'adviceDetail #sendID',
            login: 'loginPanel'
        },
        control: {
            //进入详情
            "advice dataview": {
                itemtap: function (data, index, item, record) {
                    var adviceDetail = this.getAdviceDetail();
                    if (adviceDetail == null) {
                        Ext.getStore('AdviceDetail').load({params: {'consultid': record.get('id')}});
                        //加载评论
                        var advicedetail = Ext.create('Health.view.AdviceDetail');
                        Ext.getCmp('commentList').setHtml(
                            '<div style="background:#d6e2e2; border-bottom:1px solid #b3c8c7;-webkit-box-shadow: 1px 2px 5px #b3c8c7; padding:10px 10px 0px 10px;color:#595858">' +
                                '<div style="width:100%; line-height:26px;font-size: 14px;">' + record.getData().title + '</div>' +
                                '<div style=" font-size: 12px; color: #b0b0b0; line-height: 24px; text-align: right">' +
                                '<span>用户：</span>' +
                                '<span>' + record.getData().uname + '&nbsp;&nbsp;</span>' +
                                '<span>' + [Ext.Date.format(new Date(parseInt(record.get('lasttime')) * 1000), 'Y-m-d')] + '</span>' +
                                '</div>'
                        );
                        //console.log(document.getElementById('consultid'))
                        Ext.getCmp('consultid').setHtml(record.get('id'));
                        //document.getElementById('consultid').value= record.get('id');
//                        var uid = localGet('app_Login_id');
//                        if (record.get('uid') != uid) {
//                            Ext.getCmp('sendID').hide(true);
//                        }
                        if (localGet('app_Login_status') == '2') {
                            Ext.getCmp('sendID').show(true);
                        }
                        gotoFn(advicedetail);
                        return false;
                    }
                }
            },
            //提问页面
            "askButton": {
                tap: function () {
                    var askPanel = this.getAskPanel();
                    if (askPanel == null) {
                        var uid = localGet('app_Login_id');
                        var uname = localGet('app_Login_name');
                        if (uid == null || uname == null) {
//                            Ext.Viewport.remove(this.getAdvice(), true)
//                            Ext.Viewport.remove(this.getLogin(), true)
//                            Ext.Viewport.animateActiveItem({xtype: 'loginPanel'}, {type: 'slide', direction: 'left'});
//                            return;
                            gotoFn("loginPanel");
                            return false;
                        }
                        askPanel = Ext.create('Health.view.Ask');
                    }
                    gotoFn(askPanel);
                    return false;
                }
            },
            //提问
            "asksubmit": {
                tap: function () {
//                    var cid = Ext.getCmp('cid').getHtml()
                    cid = localGet('cid');
                    console.log(cid)
                    if (cid == null) {
                        return;
                    }
                    var uid = localGet('app_Login_id');
                    var uname = localGet('app_Login_name');
                    console.log(uid, uname)
                    if (uid == null || uname == null) {
                        gotoFn("loginPanel");
                        return false;
                    }
                    var title = this.getAskText().getValue();
                    if (title == '') {
                        cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", ['内容不能为空']);
                        return;
                    }
                    Ext.Viewport.mask({xtype: 'loadmask' });
                    Ext.Ajax.request({
                        url: Global.api_url + '/cloud/1/consult_add',
                        method: 'POST',
                        params: {
                            cid: cid,
                            uid: uid,
                            uname: uname,
                            title: title,
                            privateState: Ext.getCmp('consult_disabled').getValue()
                        },
                        scope: this,
                        success: function (result) {
                            Ext.Viewport.unmask();
                            var result = Ext.decode(result.responseText);
                            if (result.Variables.Result.code == 0) {
                                Ext.getStore('Advice').load({params: {uid: uid}});
//                                var backid = Ext.Viewport.getInnerItems().length - 2;
//                                Ext.Viewport.remove(this.getAskPanel(), true);
////                                Ext.Viewport.setActiveItem(backid);
//                                Ext.Viewport.animateActiveItem({xtype: 'advice'}, {type: 'slide', direction: 'left'});
                                gotoFn("advice")
                                return false;
                            } else {
                                cordova.exec(function (message) {
                                }, function (error) {
                                }, "Messages", "showMsg", [result.Variables.Message.messagestr]);
                                return false;
                            }
                        },
                        failure: function () {
                            Ext.Viewport.unmask();
                            // console.log('请求失败', '服务器维护中....');
                            cordova.exec(function (message) {
                            }, function (error) {
                            }, "Messages", "showMsg", ['请求失败', '服务器维护中....']);
                            return false;
                        }

                    });
                }
            },
            //回复
            "sendBtn": {
                tap: function () {
                    var uid = localGet('app_Login_id');
                    var uname = localGet('app_Login_name');
                    if (uid == null || uname == null) {
                        gotoFn("loginPanel");
                        return false;
                    }
                    var message = this.getTextsend().getValue();
                    if (message == '') {
                        cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", ['内容不能为空']);
                        return;
                    }
                    var consultid = Ext.getCmp('consultid').getHtml();
                    Ext.Viewport.mask({xtype: 'loadmask' });
                    Ext.Ajax.request({
                        url: Global.api_url + '/cloud/1/consult_post_add',
                        method: 'POST',
                        params: {
                            consultid: consultid,
                            uid: uid,
                            uname: uname,
                            messages: message
                        },
                        scope: this,
                        success: function (result) {
                            Ext.Viewport.unmask();
                            var result = Ext.decode(result.responseText);
                            if (result.Variables.Result.code == 0) {
                                Ext.getStore('AdviceDetail').load({params: {'consultid': consultid}});
                                return;
                            } else {
                                // console.log(result.Variables.Message.messagestr);
                                cordova.exec(function (message) {
                                }, function (error) {
                                }, "Messages", "showMsg", [result.Variables.Message.messagestr]);
                                return false;
                            }
                        },
                        failure: function () {
                            Ext.Viewport.unmask();
                            // console.log('请求失败', '服务器维护中....');
                            cordova.exec(function (message) {
                            }, function (error) {
                            }, "Messages", "showMsg", ['请求失败', '服务器维护中....']);
                            return false;
                        }

                    })
                    //回复之后，text清空
                    this.getTextsend().reset();
                }
            },
            //显示列表
            "adviceCategory dataview": {
                itemtap: function (data, index, item, record) {
                    var advice = this.getAdvice();
                    var uid = localGet('app_Login_id');
                    if (advice == null) {
                        advice = Ext.create("Health.view.Advice");
                        if (Ext.getStore('Advice').getData().length == 0) {
                            Ext.getStore('Advice').load();
                        }
                        if (uid != null) {
                            Ext.getStore('Advice').load({params: {uid: uid}});
                        } else {
                            Ext.getStore('Advice').load();
                        }
                        Ext.getStore('Advice').filter('cid', record.get('id'));
                        localSave('cid', record.get('id'));
                        //加载咨询
                        console.log("-----------------")
                    }
                    gotoFn(advice)
                    return false;
                }
            }
        }
    }
})