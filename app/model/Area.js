Ext.define('Health.model.Area',{
	extend:'Ext.data.Model',
	config:{
        fields: ['cadid', 'id', 'name', 'status', /*'caid',*/ 'starttime', 'endtime','statusValue','explain','address','tel'],
        validations: [
            { type: 'presence', field: 'cadid', message: 'ID不能为空！'},
            { type: 'presence', field: 'id', message: '分类名称不能为空！'}
        ]
	}
})