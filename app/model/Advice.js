Ext.define('Health.model.Advice',{
    extend:'Ext.data.Model',
    config:{
        fields: ['id','cid','uid','uname','title','status','viewnum','commentnum','lasttime','privateState']
    }
})