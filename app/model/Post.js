Ext.define('Health.model.Post',{
    extend:'Ext.data.Model',
    config:{
        fields: ['id','title','user_id','lasttime','count'],
        autoLoad:true,
        proxy:{
            type:'localstorage',
            id:'myPost'
        },

        belongsTo: 'Health.model.User',
        hasMany: { model: 'Health.model.Comment', name: 'comments' }
    }
})