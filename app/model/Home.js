Ext.define('Health.model.Home', {
    extend: 'Ext.data.Model',
    config: {
        fields: ['ccid', 'name', 'display', 'cadid', 'lasttime', 'parentid','iconUrl','notread','ctype']
    }
});