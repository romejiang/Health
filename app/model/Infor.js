Ext.define('Health.model.Infor', {
    extend: 'Ext.data.Model',
    config: {
        fields: ['caid', 'ccid', 'name', 'picurl', 'lasttime', 'content','summary','notread']
    }
});