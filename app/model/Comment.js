//帖子评论
Ext.define('Health.model.Comment',{
    extend:'Ext.data.Model',
    config:{
        fields: ['id','post_id','user_id','lasttime','content','user_sex','user_age'],
        autoLoad:true,
        proxy:{
            type:'localstorage',
            id:'myComment'
        },

        belongsTo: 'Health.model.Post'
    }
})