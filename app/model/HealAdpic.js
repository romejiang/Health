Ext.define('Health.model.HealAdpic', {
    extend: 'Ext.data.Model',
    config: {
        fields: ['cadid', 'id', 'name', 'status', 'caid', 'starttime', 'endtime','statusValue','picUrl']
    }
});