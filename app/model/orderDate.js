Ext.define('Health.model.OrderDate',{
    extend:'Ext.data.Model',
    config:{
        fields: ['date','week','count','id']
    }
})