Ext.define('Health.model.AdviceCategory',{
	extend:'Ext.data.Model',
	config:{
        fields: ['id','name','display','aid','parentid','lasttime','iconUrl']
		
	}
})