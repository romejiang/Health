Ext.define('Health.model.Order', {
    extend: 'Ext.data.Model',
    config: {
        fields: ['cpid','isviewbate', 'name', 'picurl', 'explain', 'grade', 'assessnum', 'viewnum', 'material','rebate', 'term', 'lasttime', 'disabled', 'cadid', 'isorder','isorder_arr','status','category','standards','photos','recommend']
    }
});