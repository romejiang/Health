Ext.define('Health.model.AdviceDetail',{
    extend:'Ext.data.Model',
    config:{
        fields: ['id','consultid','uid','uname','status','messages','lasttime']
    }
})