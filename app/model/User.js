//用户
Ext.define('Health.model.User',{
    extend:'Ext.data.Model',
    config:{
        fields: ['id','name','mobile_num','age','sex','birthyear','birthmonth','rename','address'],
        autoLoad:true,
        proxy:{
            type:'localstorage',
            id:'myUser'
        },
        hasMany: { model: 'Health.model.Post', name: 'posts' }
    }
})