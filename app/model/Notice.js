Ext.define('Health.model.Notice', {
    extend: 'Ext.data.Model',
    config: {
        fields: ['canid','noticecontent','createtime'],
        validations: [
            { type: 'presence', field: 'id', message: '地区ID不能为空！'},
            { type: 'presence', field: 'caid', message: '所属应用ID不能为空！'},
            { type: 'format', field: 'caid', matcher: /\d+/, message: '地区ID格式不合法！'}
        ]
    }
});