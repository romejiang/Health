Ext.define('Health.store.Notice',{
	extend:'Ext.data.Store',
    requires: [
        'Ext.data.proxy.JsonP'
    ],
    config: {
        autoLoad: false,
        pageSize: 10,
        model: 'Health.model.Notice',
        proxy: {
            type: 'jsonp',
            limitParam: 'pageSize',
            url: Global.api_url + '/cloud/1/notice_district_list',
            reader: {
                type: 'json',
                rootProperty: 'Variables.data',
                totalProperty:'Variables.count'
            }
        }
    }

	
});