Ext.define('Health.store.Order', {
    extend:'Ext.data.Store',
    config: {
        autoLoad:false,
        model: 'Health.model.Order',
        proxy: {
            type: 'jsonp',
            url: Global.api_url + '/cloud/1/product_all_find',
            reader: {
                type: 'json',
                rootProperty: 'Variables.data'
            }
        }
    }
});