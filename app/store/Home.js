Ext.define('Health.store.Home',{
    extend:'Ext.data.Store',
    config: {
        autoLoad:false,
        model:'Health.model.Home',
        proxy: {
            type: 'jsonp',
            url: Global.api_url + '/cloud/1/column_all_find',
            reader: {
                type: 'json',
                rootProperty: 'Variables.data'
            }
        }
    }
})