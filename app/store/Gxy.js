
Ext.define('Health.store.Gxy',{
    extend:'Ext.data.Store',
//    requires: [
//        'Ext.data.proxy.LocalStorage'
//    ],
//    config:{
//        model:'Health.model.Gxy',
//        id:'gxyStore',
//        aoutLoad:true,
//        data: [
//            {id:"add",date:"",xueya:"",shiyan:"",height:"",weight:"",xinlv:"",maibo:"",yaowei:"",zhishu:""}
//        ],
//        proxy:{
//            type:'localstorage',
//            id:'myGxyKey'
//        }
//    }
    config: {
        autoLoad:false,
        model:'Health.model.Gxy',
        proxy: {
            type: 'jsonp',
            url: Global.api_url + '/cloud/1/feritin_all_find',
            reader: {
                type: 'json',
                rootProperty: 'Variables.data'
            }
        }
    }
});

