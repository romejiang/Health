Ext.define('Health.store.Advice',{
    extend:'Ext.data.Store',




    config:{
        model:'Health.model.Advice',
        id:'adviceStore',
        aoutLoad:false,
        proxy: {
            type: 'jsonp',
            url: Global.api_url + '/cloud/1/consult_all_find',
            reader: {
                type: 'json',
                rootProperty: 'Variables.data'
            }
        }
    }

})