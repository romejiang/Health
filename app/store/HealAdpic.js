
Ext.define('Health.store.HealAdpic', {
    extend:'Ext.data.Store',
    config: {
        model: 'Health.model.HealAdpic',
        id:"HealAdpic",
        aoutLoad:false,
        proxy: {
            type: 'jsonp',
            url: Global.api_url + '/cloud/1/adpic_district_find',
            reader: {
                type: 'json',
                rootProperty: 'Variables.data'
            }
        }
    }
});