Ext.define('Health.store.Infor',{
    extend:'Ext.data.Store',
    config: {
        autoLoad:false,
        model:'Health.model.Infor',
        proxy: {
            type: 'jsonp',
            url: Global.api_url + '/cloud/1/article_column_find',
            reader: {
                type: 'json',
                rootProperty: 'Variables.data'
            }
        }
    }

})