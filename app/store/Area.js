Ext.define('Health.store.Area',{
	extend:'Ext.data.Store',
    requires: [
        'Ext.data.proxy.JsonP'
    ],
    config: {
        autoLoad: false,
        model: 'Health.model.Area',
        proxy: {
            type: 'jsonp',
            url: Global.api_url + '/cloud/1/area_status_find',
            reader: {
                type: 'json',
                rootProperty: 'Variables.data'
            }
        }
    }

	
});