Ext.define('Health.store.User',{
    extend:'Ext.data.Store',
    requires: [
        'Ext.data.proxy.JsonP',
        'Ext.data.reader.Json'
    ],
    config:{
        model:'Health.model.User',
        id:'User',
        aoutLoad:true,
//        data: [
//            {id:'01',name:'lilj',password:'111',mobile_num:'189189',age:'26',sex:'女'}
//        ]

//        proxy:{
//            type:'localstorage',
//            id:'user'
//        }

        proxy: {
            type: 'jsonp',
            url: Global.api_url + '/cloud/1/users_info_get',
            reader: {
                type: 'json',
                rootProperty: 'Variables.data'
            }
        }
    }
})