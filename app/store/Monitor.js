
Ext.define('Health.store.Monitor',{
	extend:'Ext.data.Store',
	config:{
		model:'Health.model.Monitor',
		id:'monitorStore',
        autoLoad:false,
		data: [
            {'id': 'tnb', "picUrl": "jc_00.png" ,'title':'糖尿病'},
            {'id': 'gxy', "picUrl": "jc_01.png",'title':'高血压' }

        ]
	}
	
})