Ext.define('Health.store.AdviceCategory',{
	extend:'Ext.data.Store',
    id:'adviceCategory',
    config:{
        model:'Health.model.AdviceCategory',
        aoutLoad:false,
        proxy: {
            type: 'jsonp',
            url: Global.api_url + '/cloud/1/consult_category_all_find?aid=1',
            reader: {
                type: 'json',
                rootProperty: 'Variables.data'
            }
        }
    }
})