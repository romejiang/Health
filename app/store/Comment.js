Ext.define('Health.store.Comment',{
    extend:'Ext.data.Store',
    requires: [
        'Ext.data.proxy.LocalStorage'
    ],
    config:{
        model:'Health.model.Comment',
        id:'Comment',
        aoutLoad:true, 
        proxy:{
            type:'localstorage',
            id:'myComment'
        }
    }

})