Ext.define('Health.store.AdviceDetail',{
	extend:'Ext.data.Store',
    id:'adviceDetail',
    config:{
        model:'Health.model.AdviceDetail',
        aoutLoad:false,
        proxy: {
            type: 'jsonp',
            url: Global.api_url + '/cloud/1/consult_post_info_get',
            reader: {
                type: 'json',
                rootProperty: 'Variables.data'
            }
        }
    }
})