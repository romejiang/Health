
//糖尿病stroe
Ext.define('Health.store.Immunity',{
    extend:'Ext.data.Store',
    requires: [
        'Ext.data.proxy.LocalStorage'
    ],
    config:{
        model:'Health.model.Immunity',
        id:'immunity',
        aoutLoad:false,
        proxy: {
            type: 'jsonp',
            url: Global.api_url + '/cloud/1/immunity_data_all_find',
            reader: {
                type: 'json',
                rootProperty: 'Variables.data'
            }
        }
//        aoutLoad:false,
//        proxy:{
//            type:'localstorage',
//            id:'immunity'
//        }
    }
});

