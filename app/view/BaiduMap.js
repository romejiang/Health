/*首页*/
Ext.define('Health.view.BaiduMap', {
    extend: 'Ext.Container',
    xtype: 'baiduMap',


    config: {
        id: "baidumap",

        html: "map is here!",
        listeners: {
            initialize: function () {
                var script = document.createElement("script");
                script.src = "http://api.map.baidu.com/api?v=1.4&callback=Health.view.BaiduMap.draw";
                document.body.appendChild(script);
            }
        }

    },
    statics: {
        draw: function () {
            // var elem = Ext.select('.mapspace-inner').elements[0];
            // console.log(elem);
            // elem.style.height = '100%';
            var map = new BMap.Map('baidumap');

            map.addControl(new BMap.NavigationControl());
            map.addControl(new BMap.ScaleControl());
            // map.addControl(new BMap.OverviewMapControl());  
            // map.addControl(new BMap.MapTypeControl());  
            // map.setCurrentCity("西安");

            var sContent =
                "<h4 style='margin:0 0 5px 0;padding:0.2em 0'>有道软件（西安）技术有限公司</h4>" +
                    "<p style='margin:0;line-height:1.5;font-size:13px;text-indent:2em'>是目前西北地区专业技术能力最强,客户覆盖面最广的一家移动互联网应用专业技术开发和运营企业,本地知名机构和企业都采用公司的产品和技术方案。</p>" +
                    "</div>";

            var opts = {
                width: 350,     // 信息窗口宽度
                height: 100     // 信息窗口高度
                // title : "Hello"  // 信息窗口标题
            }


            var infoWindow = new BMap.InfoWindow(sContent, opts);  // 创建信息窗口对象

            var myGeo = new BMap.Geocoder();
            // 将地址解析结果显示在地图上，并调整地图视野  
            myGeo.getPoint("西安市汇鑫ibc", function (point) {
                if (point) {
                    map.centerAndZoom(point, 16);
                    map.addOverlay(new BMap.Marker(point));
                    map.openInfoWindow(infoWindow, point); //开启信息窗口
                }
            }, "西安市");


            // map.centerAndZoom(new BMap.Point(121.491, 31.233), 11);
        }
    }


});
