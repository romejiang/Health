/*信息列表*/
Ext.define('Health.view.InforList', {
    extend: 'Ext.Panel',
    xtype: 'inforList',

    requires:[
        'Ext.data.Store',
        'Ext.field.Search',
        "Ext.dataview.List"
    ],

    config:{
        layout:'card',
        style: 'background:#d6e2e2',
        params:{
            ccid:0,
            udid:0
        },
        items:[
            {
                xtype:'toolbar',
                ui: 'dark',
                docked:'top',
                id:'infor_title',
                items:{
                    xtype:'button',
                    text:'返回',
                    handler:gotoFn
                },
                style:'z-index:2'
            },
            {
                xtype:'list',
                style: 'background:#d6e2e2',
                pressedCls:false,
                selectedCls:false,
                store:'Infor',
                emptyText:'<div style="text-align: center;width: 100%;">暂无内容</div>',
                itemTpl:"<div class='inforList'>"+
                            "<img src='{picurl}' style=' float:left; width:70px; height:70px; -webkit-border-radius: 4px; border-radius: 4px;'/>"+
                            "<div class='inforList_title' style='margin:0 26px 0 80px; line-height:20px;height: 20px; overflow: hidden '>{name}" +
                            "<tpl if='notread!=0'>" +
                            "<img src='resources/images/5.png' height='10px' width='10px;' style='margin-left: 3px;'/>"+
                            "</tpl>" +
                            "</div>"+
                            "<div class='inforList_content' style='margin:0 26px 0 80px; line-height: 16px; height: 32px '>{summary}" +"</div>"+
                            "<img src='resources/images/go_0.png' class='inforList_go'/>"+
                            "<span class='inforList_date inforList_content'>{[Ext.Date.format(new Date(parseInt(values.lasttime) * 1000), 'Y-m-d')]}</span>"+
                    "</div>",
                listeners:{
                    painted:function(list){
                        var Infor = Ext.getStore('Infor');
                        Infor.load({params: this.up("inforList").config.params});
                    }
                }
            }
        ],listeners: {
            back: gotoFn
        }

    }

});

