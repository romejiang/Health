Ext.define('Health.view.MoreInfor', {
    extend: 'Ext.Container',
    xtype: 'moreInfor',
    config: {
        layout: 'vbox',
        items: [
            {
                docked: 'top',
                xtype: 'titlebar',
                title:'关于',
                style:'z-index:2;',
                items: {
                    text: '返回',
//                    handler: function () {
//                        Ext.Viewport.remove(this.up('moreInfor'), true);
//                    }
                    handler:gotoFn
                }
            },
            {
                xtype:'panel',
                flex:5,
                scrollable:true,
                id: 'infoHtml',
                html:'',
                cls:'immunity_content',
                style:'padding:5px 10px; line-height:26px; font-size:14px;'
            }
        ],
        listeners: {
            initialize: function () {
                Ext.Viewport.mask({ xtype: 'loadmask', message: '加载数据中...' });
                Ext.Ajax.request({
                    url: Global.api_url + '/cloud/1/article_info_get',
                    params: {caid: 85},
                    method: 'POST',
                    scope: this,
                    success: function (response) {
                        Ext.Viewport.unmask();
                        var returnText = Ext.decode(response.responseText);
                        if (returnText.Variables.Result.code == 0) {
                            Ext.getCmp('infoHtml').setHtml(
                                '<div style="padding: 5px 5px 10px 5px">' +
                                    '<div>' + returnText.Variables.data.content + '</div>' +
                                    '</div>'
                            )
                        }
                    },
                    failure: function () {//请求失败时执行操作
                        Ext.Viewport.unmask();
                        cordova.exec(function(message){}, function(error){}, "Messages", "showMsg", ['请求失败,服务器维护中...']);
                    }
                })
            }
        }
    }
});

