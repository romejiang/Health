/**
 * Created with JetBrains WebStorm.
 * User: Administrator
 * Date: 13-7-9
 * Time: 下午3:18
 * To change this template use File | Settings | File Templates.
 */
/*信息列表*/
Ext.define('Health.view.PlanList', {
    extend: 'Ext.Panel',
    xtype: 'planlist',
    count:0,
    requires: [
        'Ext.data.Store',
        'Ext.field.Search',
        "Ext.dataview.List"
    ],

    config: {
        layout: 'card',
        style: 'background:#d6e2e2;',
        items: [
            {
                xtype: 'toolbar',
                ui: 'dark',
                docked: 'top',
                title: '提醒列表',
                style: 'z-index:1',
                items: {
                    xtype: 'button',
                    text: '返回',
                    handler: gotoFn
                }
            },
            {
                xtype: 'dataview',
                style: 'background-color:#ffffff',
                pressedCls: false,
                selectedCls: false,
                id:"planListDataView",
                emptyText: '暂无内容',
                itemTpl:
                    "<div class='list_plan' style='padding: 10px;'>" +
                        //是否选中
                        "<tpl if='select==1'>" +
                            '<input type="button"  style="width:25px;height: 25px;float:left; border: none; background: url(resources/images/immunity_2.png); background-size: 25px;"  />' +
                        "<tpl else>"+
                            '<input type="button" name="select" style="width:25px;height: 25px;float:left; border: none; background: url(resources/images/immunity_1.png); background-size: 25px;"  />' +
                        "</tpl>"+
                            "<span style='color: #146d6d; display: block; line-height: 26px;'>{daymes}<br/>{message}</span>" +
                            "<span style='display: none; text-align:right;color: #146d6d;line-height: 26px;'>{key_value}</span>" +
                    "</div>"
            }
        ], listeners: {
            back: function () {
                Ext.Viewport.remove(Ext.Viewport.getActiveItem(), true);
            }
        }

    }

});

