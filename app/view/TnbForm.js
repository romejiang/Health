Ext.define('Health.view.TnbForm', {
    extend: 'Ext.form.Panel',
    xtype: 'tnbFormPanel',

    requires: [
        'Ext.data.Store',
        'Ext.form.FieldSet',
        'Ext.field.Toggle'
    ],

    config: {
        layout: 'card',
        scrollable: false,
        height: '100%',
        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                title: '我的监测',
                items: [
                    {
                        xtype: 'button',
                        text: '返回',
                        handler: gotoFn
                    },
                    {xtype: 'spacer'},
                    {
                        xtype: 'button',
                        text: '去咨询',
                        handler: function () {
                            localSave('cid', 2)
                            gotoFn("askPanel");
                            return false;
                        }
                    }
                ]
            },
            {
                xtype: 'fieldset',
                title: '糖尿病日常监测',
                id: 'tnbform',
                style: 'margin:12px;background:#f4f4f4',
                scrollable: true,
                items: [
                    {
                        xtype: 'checkboxfield',
                        label: '有无症状',
                        name: 'symptom',
                        id: 'symptom'
                    },
                    {
                        xtype: 'checkboxfield',
                        label: '多饮',
                        name: 'drinking',
                        id: 'drinking'
                    },
                    {
                        xtype: 'checkboxfield',
                        label: '多食',
                        name: 'eat',
                        id: 'eat'
                    },
                    {
                        xtype: 'checkboxfield',
                        label: '多尿',
                        name: 'urine',
                        id: 'urine'
                    },
                    {
                        xtype: 'checkboxfield',
                        label: '视力模糊',
                        name: 'vision',
                        id: 'vision'
                    },
                    {
                        xtype: 'checkboxfield',
                        label: '感染',
                        name: 'infect',
                        id: 'infect'
                    },
                    {
                        xtype: 'checkboxfield',
                        label: '手脚麻木',
                        name: 'numb',
                        id: 'numb'
                    },
                    {
                        xtype: 'checkboxfield',
                        label: '下肢浮肿',
                        name: 'edema',
                        id: 'edema'
                    },
                    {
                        xtype: 'checkboxfield',
                        label: '体重明显下降',
                        name: 'weightloss',
                        id: 'weightloss'
                    },
                    {
                        xtype: 'textfield',
                        name: 'weight',
                        id: 'weight',
                        label: '体重',
                        placeHolder: 'kg'
                    },
                    {
                        xtype: 'textfield',
                        name: 'smoke',
                        id: 'smoke',
                        label: '日吸烟量',
                        placeHolder: '支/每天'
                    },
                    {
                        xtype: 'textfield',
                        name: 'drink',
                        id: 'drink',
                        label: '日饮酒量烟量',
                        placeHolder: '毫升/每天'
                    },
                    {
                        xtype: 'panel',
                        layout: 'hbox',
                        id: 'tnbbutton',
                        hidden: false,
                        style: 'background:#f4f4f4',
                        items: [
                            {
                                xtype: 'button',
                                cls: 'form_confirm',
                                action: 'tnbFormSubmit',
                                width: '65%',
                                style: 'margin:0 2% 0 0;'
                            },
                            {
                                xtype: 'button',
                                cls: 'form_cancle',
                                action: 'formCancel',
                                width: '31%',
                                style: 'margin:0 0 0 2%;'
                            }
                        ]
                    }
                ]
            }
        ],
        listeners: {
            back: function () {
                Ext.Viewport.getActiveItem().pop();
            }
        }
    }
})