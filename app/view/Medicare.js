
/*信息详情*/
Ext.define('Health.view.Medicare', {
    extend: 'Ext.Container',
    xtype: 'medicare',

    requires:[
        'Ext.data.Store',
        'Ext.field.Search',
        "Ext.dataview.List"
    ],
    config:{
        style: 'background:#d6e2e2;',
        scrollable:true,
        items:[
            {
                xtype:'toolbar',
                ui:'dark',
                title:'居民医保',
                docked:'top',
                items:{
                    xtype:'button',
                    text:'返回',
                    handler:gotoFn
                },
                style:'z-index:2'
            },
            {
                xtype:'panel',
                style: 'background:#c1d3d3;',
                items:[
                    {
                        xtype:'fieldset',
                        flex:1,
                        id:'medicareField',
                        style: 'padding:20px 0 5px 0',
                        cls:'x-form-fieldset2',
                        items:[
                            {
                                style: 'background:#fff; ',
                                xtype:'textfield',
                                id:'medicare_name',
                                name:'name',
                                label:'姓&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名',
                                listeners:{
                                    initialize:function(){
                                        Ext.getCmp('medicare_name').setValue(localGet('medicare_name'))
                                    }
                                }

                            },
                            {
                                style: 'background:#fff',
                                xtype:'textfield',
                                label:'电话',
                                name:'tel',
                                id:'medicare_tel',
                                listeners:{
                                    initialize:function(){
                                        Ext.getCmp('medicare_tel').setValue(localGet('medicare_tel'))
                                    }
                                }
                            },
                            {
                                style: 'background:#fff',
                                xtype:'textfield',
                                label:'医保号',
                                name:'number',
                                id:'medicare_num',
                                listeners:{
                                    initialize:function(){
                                        Ext.getCmp('medicare_num').setValue(localGet('medicare_num'))
                                    }
                                }
                            },
                            {
                                style: 'margin:14px 20px 0 20px',
                                xtype:'button',
                                id:'medicare_button',
                                cls:'backButton',
                                action:'medicare',
                                text:'注册医保',
                                listeners:{
                                    initialize:function(){
                                        if(localGet('medicare_name')!=null&&localGet('medicare_tel')!=null&&localGet('medicare_num')!=null){
                                            this.setHidden(true);
                                            Ext.getCmp('medicare_name').setReadOnly(true);
                                            Ext.getCmp('medicare_tel').setReadOnly(true);
                                            Ext.getCmp('medicare_num').setReadOnly(true);
                                        }
                                    }
                                }
                            }
                        ]

                    },
                    {
                        xtype:'panel',
                        style: 'background:#d6e2e2;padding:10px 10px 0 10px',
                        flex:2,
                        id:'medicare_detail'
                    }
                ]
            }

        ]
    }
});

