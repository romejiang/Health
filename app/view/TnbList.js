
Ext.define('Health.view.TnbList', {
    extend: 'Ext.DataView',
    xtype: 'tnbListPanel',

    requires: [
        "Ext.data.Store",
        "Ext.dataview.List",
        "Ext.data.proxy.Ajax"
    ],

    config:{
        title:'我的监测',
        iconCls:'info',
        scrollable:false,
        height:'100%',
        layout: {
            type: 'card'
        },
        defaultBackButtonText:'返回',

        items:[
            {
                xtype:'toolbar',
                docked:'top',
                title:'糖尿病日常监测',
                items:{
                    xtype:'button',
                    text:'返回',
                    handler:gotoFn
                }
            },
            {
                xtype: "dataview",
                store:'Tnb',
                selectedCls:false,
                pressedCls:false,
                scrollable: true,
                itemTpl:new Ext.XTemplate(
                    '<tpl if="id== \'add\'">',
                        '<div style="width: 33%;text-align:center; float: left;">' +
                        '<div style="background:url(resources/images/from_listbg.png); margin:5px; width:98px; height:120px;padding:0px;position:relative;">'+
                    	  '<span style="display:block;padding-top:50px;font-weight:bolder; color:#eb8225; font-size:20px; text-align:center ">+</span>'+
                    	  '<span style=" font-size:12px; color:#818588; text-align:center">添加</span>'+
                            '</div>'+
                          '</div>',
                    '<tpl else>',
                    '<div style="width: 33%;text-align:center; float: left;">' +
                        '<div style="background:url(resources/images/from_listbg.png); margin:5px; width:98px; height:120px;padding:0px;position:relative;">'+
                          '<span style="display:block;padding-top:50px;font-weight:bold; color:#146d6d; font-size:14px; text-align:center ">糖尿病监测</span>'+
                          '<span style="font-size:12px; color:#818588; text-align:center">{[Ext.Date.format(new Date(parseInt(values.lasttime) * 1000), "Y-m-d")]}</span>'+
                        '</div>'+
                        '</div>',
                    '</tpl>'
                ),
                listeners:{
                    painted:function(){
                        var store=Ext.getStore('Tnb');
                        store.load({params:{
                            uid:localGet('app_Login_id'),
                            cadid:localGet('cadid')
                        },
                            callback:function(data){
                                var record={id:"add",symptom:"", drinking:"",eat:"",urine:"",vision:"",infect:"",numb:"",weightloss:"",weight:"",smoke:"",drink:"",uid:'',cadid:"",lasttime:""};
                                store.add(record);
                                store.sync();
                            }
                        })
                    }
                }
            }

        ],listeners: {
            back: gotoFn
        }

    }

});



