/*咨询列表*/
Ext.define('Health.view.Advice', {
    extend: 'Ext.Container',
    xtype: 'advice',
    request: "Ext.dataview.List",


    config: {
        title: '专家咨询',
        iconCls: 'monitor',
        scrollable: false,
        height: '100%',
        layout: 'card',
        items: [
            {
                xtype:'titlebar',
                docked:'top',
                items:[
                    {
                        xtype:'button',
                        text:'返回',
                        handler:gotoFn
                    },{
                        xtype:'button',
                        text:'提问',
                        align:'right',
                        action:'showAsk',
                        style:'background:#e68019'
                    }
                ]
            },
            {
                xtype: "list",
                title: '社区卫生',
                style:'background:url("resources/images/bg_4.png") repeat',
                pressedCls:false,
                selectedCls:false,
                cls:'x-list5',
                store: 'Advice',
                emptyText: '暂无数据',
                itemTpl:
                    '<div style=" background-color:#d6e2e2; border-top: 1px solid #ebf1f1; border-bottom:1px solid #a0bbba; padding: 8px;">' +
                        '<img src="resources/images/bg_photo.png" style="float: left; width: 44px; margin-right: 8px;" />'+
                        '<div style="overflow: auto; font-size: 14px; color: #888686; line-height: 22px;">{uname}</div>'+
                        '<div style="overflow: auto; font-size: 14px; color: #595858; line-height: 22px;">{title}</div>'+
                        '<div style="font-size: 12px; color: #b0b0b0">' +
                            '<span style="margin-left: 52px;">{[Ext.Date.format(new Date(parseInt(values.lasttime) * 1000), "Y-m-d")]}</span>' +
                            '<div style="float:right;margin-right: 10px;">{commentnum}</div>'+
                            "<tpl if='commentnum!=0'>"+
                                "<div style=' float:right; margin-right:4px;'><img src='resources/images/btn_message_show.png' height='17px' style='vertical-align: middle' /></div>"+
                            "<tpl else>"+
                                "<div style=' float:right;margin-right:4px;'><img src='resources/images/btn_message_hide.png' height='17px' style='vertical-align: middle' /></div>"+
                            "</tpl>"+
                        '</div>'+
                    '</div>'

            }

        ], listeners: {
            back: gotoFn
        }

    }
});
 

