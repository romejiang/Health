/*信息详情*/
Ext.define('Health.view.InforDetail', {
    extend: 'Ext.Panel',
    xtype: 'inforDetail',

    requires:[
        'Ext.data.Store',
        'Ext.field.Search',
        "Ext.dataview.List"
    ],

    config:{
        layout: 'vbox',
        style: 'background:#e4ebeb;',
        scrollable:true,
        items:[
            {
                xtype:'toolbar',
                ui:'dark',
                docked:'top',
                id:'detail_title',
                items:{
                    xtype:'button',
                    text:'返回',
//                    handler:function(){
//                        var backid = Ext.Viewport.getInnerItems().length;
//                        Ext.Viewport.remove(this.up('inforDetail'), true);
//                        Ext.Viewport.setActiveItem(backid - 2);
//                    }
                    handler:gotoFn
                },
                style:'z-index:2'
            },
            {
                html:'',
                name:'',
                id:'detail'
            }
        ],
        listeners: {
//            back: function () {
//                var backid = Ext.Viewport.getInnerItems().length - 2;
//                Ext.Viewport.remove(Ext.Viewport.getActiveItem(), true);
//                Ext.Viewport.setActiveItem(backid);
//                // Ext.Viewport.animateActiveItem('inforList', {type: 'slide', direction: 'right'});
//            }
            back:gotoFn
        }
    }
});

