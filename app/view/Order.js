/*信息列表*/
Ext.define('Health.view.Order', {
    extend: 'Ext.Panel',
    xtype: 'order',

    requires:[
        'Ext.data.Store',
        'Ext.field.Search',
        "Ext.dataview.List"
    ],

    config:{
        layout:'card',
        style: 'background:#d6e2e2',
        items:[
            {
                xtype:'toolbar',
                ui: 'dark',
                docked:'top',
                title:'预约',
                items:{
                    xtype:'button',
                    text:'返回',
                    handler:gotoFn
                },
                style:'z-index:2'
            },
            {
                xtype:'list',
                style: 'background:#d6e2e2',
                pressedCls:false,
                selectedCls:false,
                store:'Order',
                emptyText:'暂无内容',
                itemTpl:"<div class='inforList'>"+
                            "<img src='{picurl}' style=' float:left; width:70px; height:70px; -webkit-border-radius: 4px; border-radius: 4px;'/>"+
                            "<div class='inforList_title' style='margin:0 26px 0 80px; line-height:20px;height: 20px; overflow: hidden '>{name}</div>"+
                            "<div class='inforList_content' style='margin:0 0 0 80px; line-height: 16px; height: 48px '>{explain}</div>"+
                    "</div>",
                listeners:{
                    initialize:function(){
                        Ext.Viewport.mask({
                            xtype: 'loadmask',
                            message: '添加中...'
                        });
                        var store=Ext.getStore('Order');
                        store.load({params:{cadid:localGet('cadid')}});
                        Ext.Viewport.unmask();
                    }
                }
            }
        ],listeners:{
//            back: function () {
//                var backid = Ext.Viewport.getInnerItems().length - 2;
//                Ext.Viewport.remove(Ext.Viewport.getActiveItem(), true);
//                Ext.Viewport.setActiveItem(backid);
//            }
            back:gotoFn
        }
    }
});

