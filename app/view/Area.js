
/*社区选择*/
Ext.define('Health.view.Area', {
    extend: 'Ext.Panel',
    xtype: 'areaPanel',
    store:'areaStore',
    requires:[
        'Ext.data.Store',
        'Ext.field.Search'
    ],
    config:{
        layout:'card',
        scrollable: false,
		height:'100%',
        items:[
            {
                xtype:'toolbar',
                ui: 'dark',
                docked:'top',
                title:'选择社区',
                items:{
                    xtype:'button',
                    text:'返回',
                    handler:gotoFn
                }
            },
            {
                xtype:'list',
                cls:'x-list1',
                pressedCls:false,
                store:'Area',
                emptyText:'暂无社区医院',
                disclosure: true,
                style:'background:#c0dddc;',
                itemTpl: "<div style='height:40px;'>"+
                    "<span style='font-size: 16px; color: #707476'>{name}</span>"+
                    "</div>",
                onItemDisclosure:  {
                    handler: function (record, btn, index, e) {
                        e.stopEvent();
                        var popmap = Ext.create('Health.view.PopMap');
                        Ext.Viewport.animateActiveItem(popmap, {type: 'slide', direction: 'left'});
                        popmap.search(record.get('name'));

                    }
                },
                listeners:{
                    initialize:function(){
                        var store=Ext.getStore('Area');
                        store.load({params:{caid:1}});
                    }
                }
            }
        ],
        listeners:{
//            back: function () {
//                Ext.Viewport.remove(Ext.Viewport.getActiveItem(), true);
//            }
            back:gotoFn
        }
    }


});



