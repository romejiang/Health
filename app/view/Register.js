
Ext.define('Health.view.Register', {
    extend: 'Ext.Panel',
    xtype: 'registerPanel',
    align:'center',
    config:{
        title:'首页',
        iconCls:'home',
        scrollable: true,
		height:'100%',
        style:'background:url(resources/images/bodybg.png) repeat-x; ',
        items:[
            {
                xtype:'image',
                src:'resources/images/icon_login.png',
                height:'162px',
                width:'195px',
                margin:'22px auto 0px auto'
            },
            {
                cls:'loginForm',
                style:'margin:3px 22px',
                items:[
                    {
                        xtype:'fieldset',
                        style:'padding-top:2px;',
                        items:[
                            {
                                xtype: 'textfield',
                                name: 'name',
                                id:'name',
                                label: '姓名'
                            },
                            {
                                xtype: 'textfield',
                                name: 'mobile_num',
                                id:'tel',
                                label: '电话'
                            },
                            {
                                xtype: 'passwordfield',
                                name: 'password',
                                id:'password',
                                label: '密码'
                            },
                            {
                                xtype: 'hiddenfield',
                                name: 'repassword',
                                id: 'repassword'
                            },
                            {
                                xtype: 'hiddenfield',
                                name: 'caid',
                                id: 'hidden_caid'
                            }

                        ]
                    }
                ]
            },
            {
                xtype:'panel',
                style:' background:transparent;width:270px;margin:10px auto;',
                layout:'hbox',
                items:[
                    {
                        xtype:'button',
                        cls:'register',
                        width:'63%',
                        style:'margin:0 2% 0 0;height:45px;',
                        action:'registerUser'
                    },
                    {
                        xtype:'button',
                        cls:'register_cancle',
                        width:'33%',
                        style:'margin:0 0 0 2%;height:45px;',
                        handler:gotoFn
                    }
                ]
            }

        ],listeners: {
            back: gotoFn
        }
    }

});



