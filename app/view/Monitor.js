
Ext.define('Health.view.Monitor', {
    extend: 'Ext.Panel',
    xtype: 'monitorPanel',

    requires: [
        "Ext.data.Store",
        "Ext.dataview.List",
        "Ext.data.proxy.Ajax"
    ],

    config:{
        title:'我的监测',
        iconCls:'monitor',
        layout: {
            type: 'card'
        },
        items:[
            {
                xtype:'toolbar',
                docked:'top',
                title:'我的监测',
                items:[
                    {xtype: 'spacer'},
                    {
                        xtype:'button',
                        text:'说明',
                        handler:function(){

                            Ext.Viewport.animateActiveItem('sb', {type: 'slide', direction: 'left'});
                            Ext.getCmp('sbDetail').setHtml(
                                '<div style="padding: 5px 10px 10px 10px">' +
                                    '<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户选择某一种病症，如“糖尿病”，点击添加，根据出现的提示列表，如实填写表中各项，确认完成，糖尿病患者的当日身体状况就会保存在“糖尿病”栏目下，并能够实时上传到社区医院建立医疗档案。根据这些保存下来的监测数据，用户可用来自我了解病症指标的波动情况，同时，医生根据该数据，依据国家居民慢性病治疗保障政策为用户提供科学的治疗。</div>' +
                                    '</div>'
                            )
                        }
                    }
                ]
            },
            {
                xtype: "dataview",
                title:'社区卫生',
                store:'Monitor',
                itemTpl:
                "<div style='text-align:center; float: left; width: 33%;padding-top: 6px'>" +
                    "<img src='resources/images/{picUrl}' height='70px' width='70px'/>" +
                    "<div class='home_font1'>{title}</div>" +
                    "</div>",
                listeners:{

                    painted:function(){
                        Ext.getStore("Monitor").load();
                    }
                }

            }

        ]

    }

});



