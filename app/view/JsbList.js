
Ext.define('Health.view.JsbList', {
    extend: 'Ext.DataView',
    xtype: 'jsbListPanel',

    requires: [
        "Ext.data.Store",
        "Ext.dataview.List",
        "Ext.data.proxy.Ajax"
    ],

    config:{
        title:'我的监测',
        iconCls:'info',
        scrollable: false,
		height:'100%',
        layout: {
            type: 'card'
        },

        defaultBackButtonText:'返回',

        items:[
            {
                xtype:'toolbar',
                docked:'top',
                title:'精神病日常监测',
                items:{
                    xtype:'button',
                    text:'返回',
                    handler:function(){
                        Ext.Viewport.animateActiveItem('mainPanel', {type: 'slide', direction: 'left'});
                    }
                }
            },
            {
                xtype: "dataview",
                store:'Jsb',
                selectedCls:false,
                pressedCls:false,
                scrollable: true,
                itemTpl:new Ext.XTemplate(
                    '<tpl if="id== \'add\'">',
                    '<div style="background:url(resources/images/from_listbg.png); margin:5px; width:98px; height:120px;padding:0px;position:relative;">'+
                        '<span style=" width:98px; position:absolute; top:50px; font-weight:bolder; color:#eb8225; font-size:20px; text-align:center ">+</span>'+
                        '<span style="width:98px; position:absolute; bottom:30px; font-size:12px; color:#818588; text-align:center">添加</span>'+
                        '</div>',
                    '<tpl else>',
                    '<div style="background:url(resources/images/from_listbg.png); margin:5px; width:98px; height:120px;padding:0px;position:relative;">'+
                        '<span style=" width:98px; position:absolute; top:50px; font-weight:bold; color:#146d6d; font-size:14px; text-align:center ">精神病监测</span>'+
                        '<span style="width:98px; position:absolute; bottom:30px; font-size:12px; color:#818588; text-align:center">{date}</span>'+
                        '</div>',
                    '</tpl>'
                ),
                inline: {
                    wrap: true
                }
            }
        ],
        listeners: {
            back: function () {
                Ext.Viewport.getActiveItem().pop();
            }
        }
    }
});



