/*首页*/
Ext.define('Health.view.Home', {
    extend: 'Ext.Panel',
    xtype: 'homePanel',

    config:{
        title:'首页',
        iconCls:'myhome',
        items:[
            {
                xtype:'toolbar',
                ui: 'dark',
                title:'社区卫生',
                docked:'top',
                items:[
                    {xtype: 'spacer'},
                    {
                        xtype:'button',
                        text:'账户',
                        handler:function(){
                            gotoFn("account");
                            return false;
                        }
                    }
                ]
            },
            {
                xtype: "dataview",
                title:'社区卫生',
                store:'Home',
                scrollable: true,
                height:'100%',
                style:'padding:10px 0',
                itemTpl:
                    "<div style='text-align:center; float: left; width: 33%;padding-top: 6px; position: relative'>" +
                        "<tpl if='notread!=0'>" +
                        "<span style='position: absolute; left: 50%;top:6px;margin-left: 16px; background: #df0000; color: #fff; border-radius: 8px; min-width: 18px;max-width:30px;font-size: 12px; padding: 0px 4px;'>{notread}</span>" +
                        "</tpl>" +
                        "<img src='{iconUrl}' height='70px' width='70px'/>" +
                        "<div class='home_font1'>{name}</div>" +
                    "</div>",
                listeners: {
                    painted: function() {
                        var Home = Ext.getStore('Home');
                        console.log(localGet('cadid'))
                        Home.load({params: {cadid:localGet('cadid'),udid:localGet('udid')} });
                        Home.removeAt(Home.find('ccid', '61'));
                    }
                }
            }
        ]
    }


});



