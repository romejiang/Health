Ext.define('Health.view.OrderForm', {
    extend: 'Ext.Panel',
    xtype: 'orderForm',

    requires: [
        'Ext.data.Store',
        'Ext.form.FieldSet',
        'Ext.field.Toggle'
    ],

    config: {
        layout:'card',
        items: [
            {
                xtype:'toolbar',
                docked:'top',
                title:'预约',
                items:{
                    xtype:'button',
                    text:'返回',
                    handler:gotoFn
                }
            },
            {
                xtype: 'fieldset',
                id:'orderfieldset',
                title: '预约',
                style:'margin:12px;background:#f4f4f4',
                scrollable:true,
                items: [
                    {
                        xtype: 'textfield',
                        name: 'orderer',
                        label: '姓名',
                        id:'orderer'
                    },
//                    {
//                        xtype: 'textfield',
//                        name: 'picktype',
//                        id: 'picktype',
//                        label: '性别'
//                    },

                    {
                        xtype: 'togglefield',
                        name: 'picktype',
                        label: '性别',
                        id:'picktype',
                        style:'position:relative',
                        html:'<div style="position:absolute;top:10px;left:15px;font-size:14px;color:#818588 ">男</div><div style="position:absolute;top:10px;left:112px;font-size:14px;color:#818588">女</div>'
                    },
                    {
                        xtype: 'textfield',
                        name: 'otel',
                        id: 'otel',
                        label: '电话'
                    },
                    {
                        xtype: 'textfield',
                        name: 'lasttime',
                        id: 'lasttime',
                        label: '时间',
                        readOnly:true
                    },
                    {
                        xtype:'panel',
                        layout:'hbox',
                        style:'background:#f4f4f4',
                        items:[
                            {
                                xtype: 'button',
                                cls: 'form_confirm',
                                action: 'orderSubmit',
                                width: '65%',
                                style: 'margin:0 2% 0 0;'
                            },
                            {
                                xtype: 'button',
                                cls:'form_cancle',
                                width: '31%',
                                style: 'margin:0 0 0 2%;',
                                handler:gotoFn
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'fieldset',
                id:'detailfieldset',
                title: '预约信息',
                style:'margin:12px;background:#f4f4f4;',
                scrollable:true,
                hidden:true,
                items:{
                    style:'margin:12px;background:#f4f4f4;font-size:14px; color:#6c6b6b;line-height:26px;',
                    xtype:'panel',
                    id:'order_detail'
                }
            }
        ],
        listeners: {
            listeners:{
                back:gotoFn
            }
        }
    }
})