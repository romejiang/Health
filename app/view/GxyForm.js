Ext.define('Health.view.GxyForm', {
	extend:'Ext.form.Panel',
	xtype:'gxyFormPanel',

	requires:[
        'Ext.data.Store',
        'Ext.form.FieldSet',
        'Ext.field.Toggle'
    ],

    config:{
        layout:'card',
        scrollable:false,
        height:'100%',
        items:
        [
            {
                xtype:'toolbar',
                docked:'top',
                title:'我的监测',
                items:[{
                    xtype:'button',
                    text:'返回',
                    handler:gotoFn
                    },{xtype:'spacer'},
                    {
                        xtype:'button',
                        text:'去咨询',
                        handler:function(){
                            localSave('cid',2)
                            gotoFn("askPanel");
                            return false;
                        }
                    }]
            },
            {
                xtype: 'fieldset',
                title: '高血压病日常监测',
                style:'margin:12px;background:#f4f4f4',
                scrollable:true,
                items:
                [
                    {
                        xtype:'checkboxfield',
                        label:'有无症状',
                        name:'symptom',
                        id:'gxy_symptom'
                    },
                    {
                        xtype:'checkboxfield',
                        label:'头痛头晕',
                        name:'dizzy',
                        id:'gxy_dizzy'
                    },
                    {
                        xtype:'checkboxfield',
                        label:'眼花耳鸣',
                        name:'tinnitus',
                        id:'gxy_tinnitus'
                    },
                    {
                        xtype:'checkboxfield',
                        label:'呼吸困难',
                        name:'dyspnea',
                        id:'gxy_dyspnea'
                    },
                    {
                        xtype:'checkboxfield',
                        label:'心悸胸闷 ',
                        name:'xiongmen',
                        id:'gxy_xiongmen'
                    },
                    {
                        xtype:'checkboxfield',
                        label:'鼻衄出血不止',
                        name:'bleed',
                        id:'gxy_bleed'
                    },
                    {
                        xtype:'checkboxfield',
                        label:'四肢发麻',
                        name:'numb',
                        id:'gxy_numb'
                    },
                    {
                        xtype:'checkboxfield',
                        label:'下肢水肿',
                        name:'edema',
                        id:'gxy_edema'
                    },
                    {
                        xtype: 'textfield',
                        name: 'weight',
                        id:'gxy_weight',
                        label: '体重',
                        placeHolder: 'kg'
                    },
                    {
                        xtype: 'textfield',
                        name: 'smoke',
                        id:'gxy_smoke',
                        label: '日吸烟量',
                        placeHolder: '支/每天'
                    },
                    {
                        xtype: 'textfield',
                        name: 'drink',
                        id:'gxy_drink',
                        label: '日饮酒量烟量',
                        placeHolder: '毫升/每天'
                    },
                    {
                        xtype:'panel',
                        layout:'hbox',
                        id:'gxybutton',
                        style:'background:#f4f4f4',
                        items:[
                            {
                                xtype:'button',
                                cls:'form_confirm',
//                                text:'提交',
                                action:'gxyFormSubmit',
                                width:'65%',
                                style:'margin:0 2% 0 0;height:45px;'
                            },
                            {
                                xtype:'button',
//                                text:'取消',
                                cls:'form_cancle',
                                action:'gxyformCancel',
                                width:'31%',
                                style:'margin:0 2% 0 0;height:45px;'
                            }
                        ]
                    }
                ]
            }
        ],
        listeners: {
            back: function () {
                Ext.Viewport.getActiveItem().pop();
            }
        }
    }
})