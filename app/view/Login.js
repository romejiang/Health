
Ext.define('Health.view.Login', {
    extend: 'Ext.Panel',
    xtype: 'loginPanel',

    requires:[
        'Ext.Img',
        'Ext.form.FieldSet',
        'Ext.field.Password'
    ],

    config:{
        scrollable: true,
        items:[
            {
                xtype:'image',
                src:'resources/images/icon_login.png',
                height:'182px',
                width:'178px',
                margin:'22px auto 6px auto'
            },
            {
                cls:'loginForm',
                xtype:'panel',
                style:'margin:3px 22px',
                items:[
                    {
                        xtype:'fieldset',
                        style:'padding-top:2px;',
                        items:[
                            {
                                xtype: 'textfield',
                                name: 'mobile_num',
                                id:'telField',
                                // value:"18392121466",
                                label: '电话'
                            },
                            {
                                xtype: 'passwordfield',
                                name: 'password',
                                id:'passwordlField',
                                label: '密码',
                                // value:"000000",
                                hidden:false
                            },
                            {
                                xtype: 'textfield',
                                name: 'password',
                                id:'showpassword',
                                label: '密码',
                                hidden:true
                            },
                            {
                                xtype: 'hiddenfield',
                                name: 'caid',
                                id: 'hidden_caidlField'
                            }
                        ]
                    }
                ]
            },
            {
                layout:'hbox',
                style:'margin:0 auto; font-size:12px;padding:10px 0px',
                items:[
                    {
                        html:'<div style="text-align: right">显示密码</div>',
                        style:'width:50%;color:#fff;text-align:right!important;padding-right:30px;border-right:1px solid #fff',
                        listeners: {
                            painted: function (label) {
                                label.on('tap', function () {
                                    var flag=Ext.getCmp('passwordlField').getHidden();
                                    if(!flag){
                                        Ext.getCmp('passwordlField').setHidden(true);
                                        Ext.getCmp('showpassword').setHidden(false);
                                        Ext.getCmp('showpassword').setValue(Ext.getCmp('passwordlField').getValue());
                                        this.setHtml('<div style="text-align: right">隐藏密码</div>')
                                    }else{
                                        Ext.getCmp('passwordlField').setHidden(false);
                                        Ext.getCmp('showpassword').setHidden(true);
                                        Ext.getCmp('passwordlField').setValue(Ext.getCmp('showpassword').getValue());
                                        this.setHtml('<div style="text-align: right">显示密码</div>')
                                    }
                                }, this);
                            }
                        }
                    },
                    {
                        html:'<div>找回密码</div>',
                        style:'width:50%;color:#fff;text-align:left;padding-left:30px',
                        listeners: {
                            painted: function (label) {
                                label.on('tap', function () {
                                    Ext.Viewport.animateActiveItem('findPassword', {type: 'slide', direction: 'left'});
                                    return false;
                                }, this);
                            }
                        }
                    }
                ]
            },
            {
                xtype:'panel',
                style:'background:transparent;width:270px; margin:0px auto;',
                items:{
                     xtype:'button',
                    cls:'login_confirm',
                    style:'100%',
                    action:'loginSubmit'
                }
            },
            {
                xtype:'panel',
                style:' background:transparent;width:270px;margin:0px auto;',
                layout:'hbox',
                items:[
                    {
                        xtype:'button',
                        action:'showrRegister',
                        cls:'login_reg',
                        width:'57%',
                        style:'margin:0 2% 0 0;height:45px;'
                    },
                    {
                        xtype:'button',
                        cls:'login_cancle',
                        action:'loginCancle',
                        width:'41%',
                        style:'margin:0;height:45px;'
                    }
                ]
            }

        ], listeners: {
//            back: function () {
//                Ext.Viewport.remove(Ext.Viewport.getActiveItem(), true);
//            }
            back:gotoFn
        }
    }

});



