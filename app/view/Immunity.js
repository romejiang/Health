
/*信息详情*/
Ext.define('Health.view.Immunity', {
    extend: 'Ext.form.Panel',
    xtype: 'immunity',

    requires:[
        'Ext.data.Store',
        'Ext.field.Search',
        "Ext.dataview.List"
    ],
    config:{
        style: 'background:#d6e2e2;',
        scrollable:true,
        items:[
            {
                xtype:'toolbar',
                ui:'dark',
                title:'计划免疫',
                docked:'top',
                items:{
                    xtype:'button',
                    text:'返回',
                    handler:gotoFn
                },
                style:'z-index:2'
            },
            {
                xtype:'panel',
                style: 'background:#c1d3d3;',
                items:[
                    {
                        xtype:'fieldset',
                        flex:1,
                        style: 'padding:20px 0 5px 0',
                        cls:'x-form-fieldset2',
                        items:[
                            {
                                style: 'background:#fff; ',
                                xtype:'textfield',
                                label:'名&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;称',
                                id:'immunity_name',
                                placeHolder:"宝宝",
                                listeners:{
                                    initialize:function(){
                                        Ext.getCmp('immunity_name').setValue(localGet('immunity_name'));
                                    }
                                }
                            },
                            {
                                xtype: 'datepickerfield',
                                style: 'background:#fff; ',
                                name: 'takedate',
                                id: 'immunity_birthday',
                                label: '出生日期',
                                dateFormat: 'Y-m-d',
//                                value: new Date((new Date().getFullYear()-1)+"-7-15"),
                                picker: {
                                    yearFrom: '2000',
                                    slotOrder: ['year', 'month', 'day'],
                                    doneButton: {text: '完成'},
                                    cancelButton: {text: '取消',
                                    listeners:{
                                        tap:function(){
                                            Ext.getCmp("immunity_birthday").setValue();
                                        }
                                    }}
                                },
                                listeners:{
                                    focus:function(){
                                        var birth=localGet('immunity_birthday')
                                        if(!birth||birth==null){
                                            this.setValue(new Date((new Date().getFullYear()-1)+"-7-15"));
                                        }
                                    }
                                }

                            },
                            {
                                xtype: 'datepickerfield',
                                style: 'background:#fff; ',
                                label: '上次接种时间',
                                id:'last_date',
                                name:'last_date',
                                dateFormat: 'Y-m-d',
//                                value: new Date(),
                                picker: {
                                    yearFrom: '2000',
                                    slotOrder: ['year', 'month', 'day'],
                                    doneButton: {text: '完成'},
                                    cancelButton: {text: '取消',
                                    listeners:{
                                        tap:function(){
                                            Ext.getCmp("last_date").setValue();
                                        }
                                    }}
                                },
                                listeners:{
                                    focus:function(){
                                        var last_date=localGet('last_date')
                                        if(!last_date||last_date==null){
                                            var d=new Date();
                                            this.setValue(new Date(d.getFullYear() + "-" + d.getMonth() + "-15"));
                                        }
                                    }
                                }
                            },
                            {
                                xtype: 'selectfield',
                                id:'last_vaccine_id',
                                label: '上次接种疫苗',
                                name:'last_vaccine_id',
                                store:'Immunity',
                                placeHolder:'上次接种疫苗',
                                valueField:'id',
                                displayField:'name',
                                value:localGet('last_vaccine_id'),
//                                defaultPhonePickerConfig: {
//                                    doneButton:'完成',
//                                    cancelButton:'取消'
//                                },
                                picker: {
                                    doneButton: {text: '完成'},
                                    cancelButton: {text: '取消'}
                                },
                                listeners:{
                                    initialize:function(){
                                        Ext.getStore('Immunity').load();
                                    }
                                }
                            },
                            {
                                xtype:'button',
                                cls:'backButton',
                                style: 'margin:14px 20px 0 20px',
                                text:'查询',
                                action:'immunitySearch'
                            }
                        ]
                    },
                    {
                        xtype:'panel',
                        id:'immunity_detail',
                        style: 'background:#d6e2e2;padding:10px 10px 0 10px',
                        flex:2
                    }
                ]
            }

        ],
        listeners:{
            painted:function(){
                if(!Ext.isEmpty(localGet('last_date'))){
                    Ext.getCmp('last_date').setValue(new Date(localGet('last_date')));
                }
                if(!Ext.isEmpty(localGet('last_vaccine_id'))){
                    Ext.getCmp('last_vaccine_id').setValue(localGet('last_vaccine_id'));
                }
                if(!Ext.isEmpty(localGet('immunity_birthday'))){
                    Ext.getCmp('immunity_birthday').setValue(new Date(localGet('immunity_birthday')));
                }
            }
        }
    }
});

