Ext.define('Health.view.Index', {
    extend: 'Ext.Panel',
    xtype: 'index',
    requires: [
        'Ext.carousel.Carousel'
    ],
    config: {
        layout: 'vbox',
        scrollable: {
            direction: 'vertical',
            directionLock: true
        },
        style:'background:#c0dddb',
        items: [
            {
                xtype: 'toolbar',
                title:'社区卫生',
                docked: 'top',
                items:[
                    {xtype: 'spacer'},
                    {
                        xtype:'button',
                        text:'账户',
                        handler:function(){
                            gotoFn("account");
                            return false;
                        }
                    }
                ]
            },
            {
                xtype: 'panel',
                store: 'HealAdpic',
                id:'healAdpic',
                style: 'margin:10px;background:#c0dddb',
                listeners:{
                    painted:function(carousel){
                        this.setHeight(Global.d_w / 3 * 2);
                        var store=Ext.getStore('HealAdpic');
                        store.load({
                            params: {cadid:localGet('cadid')},
                            callback: function (data) {
                                var myAdPicItems = [];
                                dataLength = data.length;
                                Ext.each(data, function (store) {
                                    var img = store.getData();
                                    Ext.each(img, function (item) {
                                        myAdPicItems.push({html: "<img src='" + item.picUrl + "' style='width:100%;height:100%; border-radius:12px ;' / >"})
                                    })
                                });
                                Ext.getCmp('healAdpic').setItems(myAdPicItems);
                            }
                        })
                    }
                }
            },
            {
                xtype: 'panel',
                style:'background:#c0dddb;padding:0px 10px',
                items: [
                    {
                        html: '<span style="padding-left: 20px"><img src="resources/images/index_tel.png" style="height: 24px;vertical-align: middle;"></span><span style="padding-left: 10px" id="index_tel"></span>',
                        cls:'immunity_content',
                        style:'border-top:1px solid #99b7b6; line-height:30px; font-size:14px',
                        listeners: {
                            painted: function (label) {
                                label.on('tap', function () {
                                    cordova.exec(function (successCallback) {
                                    }, function (errorCallback) {
                                    }, "Redirect", "callNumber", ['029-8423546']);
                                    return false;
                                }, this)
                            }
                        }
                    },
                    {
                        html: '<span style="padding-left: 20px"><img src="resources/images/index_map.png" style="height: 24px;vertical-align: middle;"></span><span style="padding-left: 10px" id="index_address"></span>',
                        cls:'immunity_content',
                        style:'border-bottom:1px solid #99b7b6; line-height:30px;font-size:14px',
                        listeners: {
                            painted: function (label) {
                                label.on('tap', function () { 
                                    var value=document.getElementById('index_address').innerText; 
                                    var popmap = Ext.create('Health.view.PopMap')
                                    Ext.Viewport.animateActiveItem(popmap, {type: 'slide', direction: 'left'});
                                    popmap.search(value);
                                    return false;
                                }, this)
                            }
                        }
                    }
                ]
            },
            {
                xtype: 'panel',
                style:'background:#c0dddb;padding:10px',
                cls: 'immunity_content',
                id:'area_info'
            }

        ],
        listeners:{
            painted:function(){
                Ext.Ajax.request({
                    url: Global.api_url + '/cloud/1/area_status_info',
                    params: {cadid:localGet('cadid')},
                    method: 'POST',
                    scope: this,
                    success: function (response) {
                       // Ext.Viewport.unmask();
                        var returnText = Ext.decode(response.responseText);
                        if (returnText.Variables.Result.code == 0) {
                            localSave('address', returnText.Variables.data.address);//设置
                            document.getElementById('index_tel').innerText=returnText.Variables.data.tel;
                            document.getElementById('index_address').innerText=returnText.Variables.data.address;
                            Ext.getCmp('area_info').setHtml(returnText.Variables.data.explain)
                        }
                    },
                    failure: function () {//请求失败时执行操作
                     //   Ext.Viewport.unmask();
                        cordova.exec(function(message){}, function(error){}, "Messages", "showMsg", ['请求失败,服务器维护中...']);
                    }
                })
            }
        }

    }
});
