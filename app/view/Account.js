Ext.define('Health.view.Account', {
    extend: 'Ext.Panel',
    xtype: 'account',

    requires: [
        "Ext.data.Store",
        "Ext.dataview.List",
        "Ext.data.proxy.Ajax",
        'Ext.form.FieldSet',
        'Ext.Label'
    ],
    config: {
        title: '账户',
        iconCls: 'mymore ',
        scrollable: false,
        height: '100%',
        layout: {
            type: 'card'
        },

        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                title: '账户',
                style: 'z-index:2',
                items: {
                    xtype: 'button',
                    text: '返回',
                    handler: gotoFn
                }
            },
            {
                xtype: 'panel',
                style: 'background:#d6e2e2;',
                items: [
                    {
                        xtype: 'fieldset',
                        style: 'margin-top:10px;',
                        //cls: 'moreFrom',
                        algin: 'center',
                        items: [
                            {
                                id: 'account_Area',
                                xtype:"button",
                                cls:"area_btn"
                            },{
                                xtype:"button",
                                cls:"changeAreaBtn",
                                handler:function(){
                                    gotoFn("areaPanel");
                                }
                            },
                            {
                                id: 'account_Login',
                                xtype:"button",
                                cls:"login_btn",
                                handler:function(){
                                    if (localGet("app_Login_name") != null) {
                                        Ext.MessageBox.YESNO =
                                            [
                                                {text: '是', itemId: 'yes', ui: 'action'},
                                                {text: '否',  itemId: 'no', ui: 'action'}
                                            ];
                                        Ext.Msg.confirm('是否退出', '', function (data) {
                                            if (data == 'yes') {
                                                Ext.Viewport.mask({
                                                    xtype: 'loadmask',
                                                    message: '退出中...'
                                                });
                                                Ext.Ajax.request({
                                                    url: Global.api_url + '/cloud/1/users_logout',
                                                    method: 'POST',
                                                    scope: this,
                                                    success: function (response) {
                                                        Ext.Viewport.unmask();
                                                        var returnText = Ext.decode(response.responseText);

                                                        cordova.exec(function (message) {
                                                        }, function (error) {
                                                        }, "Messages", "showMsg", [returnText.Variables.Message.messagestr]);
                                                        if (returnText.Variables.Result.code == 0) {
                                                            localRemove('app_Login_id');
                                                            localRemove('app_Login_status');
                                                            localRemove('app_Login_name');
                                                            localRemove('app_Login_avatar');
                                                            Ext.getCmp('account_Login').setHidden(true);
                                                            Ext.getCmp('account_Area').setHtml(
                                                                '<div class="moreFrom2 moreFont1" style=" border-radius:8px">' +
                                                                    '<span>' + localGet("app_name") + '</span>' +
                                                                    '</div>'
                                                            );
                                                            Ext.getCmp('account_button').setHidden(false)
                                                        }
                                                    },
                                                    failure: function () {//请求失败时执行操作
                                                        Ext.Viewport.unmask();
                                                        cordova.exec(function (message) {
                                                        }, function (error) {
                                                        }, "Messages", "showMsg", ['请求失败', '服务器维护中....']);
                                                        return false;
                                                    }
                                                })
                                            }
                                        }, this);
                                    } else {
                                        Ext.getCmp('account_button').setHidden(true)
                                    }
                                    return false;
                                }
                            },
                            {
                                xtype: 'button',
                                cls: 'backButton',
                                id: 'account_button',
                                style: 'margin:15px 0 0 0;height:40px; line-height:40px;font-size:18px',
                                text: '登陆',
                                listeners: {
                                    painted: function () {
                                        if (localGet("app_Login_name") != null) {
                                            this.setHidden(true);
                                            Ext.getCmp('account_Login').setHidden(false)
                                            Ext.getCmp('account_Area').setHtml(
                                                '<div class="moreFrom2 moreFont1" style=" border-radius:8px;">' +
                                                    '<span>' + localGet("app_name") + '</span>' +
                                                    '</div>'
                                            );
                                            Ext.getCmp('account_Login').setHtml(
                                                '<div class="moreFrom2 moreFont1" style=" border-radius:8px">' +
                                                    '<span>' + localGet("app_Login_name") + '</span>' +
                                                    '<input type="button" style=" height: 22px;width:61px;border:0;background: url(resources/images/button_3.png); background-size: 61px 22px; float:right; margin-top:16px" />' +
                                                    '</div>'
                                            );
                                        } else {
                                            this.setHidden(false);
                                            Ext.getCmp('account_Area').setHtml(
                                                '<div class="moreFrom2 moreFont1" style=" border-radius:8px;">' +
                                                    '<span>' + localGet("app_name") + '</span>' +
                                                    '</div>'
                                            );
                                            Ext.getCmp('account_Login').setHidden(true)
                                        }
                                    },
                                    tap: function () {
//                                        Ext.Viewport.animateActiveItem('loginPanel', {type: 'slide', direction: 'left'});
//                                        Ext.Viewport.remove('account', true);
                                        gotoFn("loginPanel");
                                    }
                                }
                            }
                        ]
                    }
                ]
            }
        ]
    }
});



