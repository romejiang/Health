/*咨询列表*/
Ext.define('Health.view.AdviceCategory', {
    extend: 'Ext.Panel',
    xtype: 'adviceCategory',

    requires: [
        "Ext.data.Store"
    ],
    config: {
        layout: 'card',
        title: '健康交流',
        items: [
            {
                xtype:'toolbar',
                title:'健康交流',
                docked:'top',
                items:[
                    {xtype: 'spacer'},
                    {
                        xtype:'button',
                        text:'账户',
                        handler:function(){
                            gotoFn("account");
                            return false;
                        }
                    }
                ]
            },
            {
                xtype: "dataview",
                title: '社区卫生',
                store: 'AdviceCategory',
                emptyText: '暂无数据',
                itemTpl:
                "<div style='text-align:center; float: left; width: 33%;padding-top: 6px'>" +
                    "<img src='{iconUrl}' height='70px' width='70px'/>" +
                    "<div class='home_font1'>{name}</div>" +
                    "</div>",
                listeners: {
                    initialize: function () {
                        if (Ext.getStore('AdviceCategory').getData().length == 0) {
                            Ext.getStore('AdviceCategory').load();
                        }
                    }
                }

            }

        ]

    }

});



