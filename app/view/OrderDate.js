/*信息列表*/
Ext.define('Health.view.OrderDate', {
    extend: 'Ext.Panel',
    xtype: 'orderDate',

    requires:[
        'Ext.data.Store',
        'Ext.field.Search',
        "Ext.dataview.List"
    ],

    config:{
        layout:'card',
        style: 'background:#d6e2e2',
        items:[
            {
                xtype:'toolbar',
                ui: 'dark',
                title:'预约',
                docked:'top',
                items:{
                    xtype:'button',
                    text:'返回',
                    handler:function(){
                        var store=Ext.getStore('OrderDate');
                        store.removeAll();
                        store.sync();
//                        Ext.Viewport.remove(this.up('orderDate'), true);
                        gotoFn();
                    }
                },
                style:'z-index:2'
            },
            {
                xtype:'list',
                style: 'background:#d6e2e2',
                pressedCls:false,
                selectedCls:false,
                store:'OrderDate',
                emptyText:'该医生最近一周不上班',
                itemTpl:"<div class='inforList' style='height: 40px;'>"+
                            "<span class='inforList_title' style='margin-right: 10px;'>{date}</span>" +
                            "<span class='inforList_title' style='margin-right: 10px;'>{week}</span>" +
                            "<span style='background: #2bb3a4; border-radius: 8px;line-height: 16px; padding:0 5px'>{count}/10</span>" +
                            "<tpl if='count<10'>" +
                                "<input type='button' class='order_img' style=' background:url(resources/images/order_1.png);background-size: cover' name='click_order' />"+
                            "<tpl else>"+
                                "<input type='button' class='order_img' style=' background:url(resources/images/order_0.png);background-size: cover' />"+
                            "</tpl>"+
                    "</div>"
            }
        ],listeners:{
//            back: function () {
//                var backid = Ext.Viewport.getInnerItems().length - 2;
//                Ext.Viewport.remove(Ext.Viewport.getActiveItem(), true);
//                Ext.Viewport.setActiveItem(backid);
//            }
            back:gotoFn
        }

    }

});

