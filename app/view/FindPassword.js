/**
 *
 */
Ext.define('Health.view.FindPassword', {
    extend: 'Ext.form.Panel',
    xtype: 'findPassword',
    requires: [
        'Ext.form.Panel',
        'Ext.form.FieldSet',
        'Ext.field.Password'
    ],
    config: {
        scrollable: {
            direction: 'vertical',
            directionLock: false
        },
        layout: 'vbox',
        items: [
            {
                docked: 'top',
                xtype: 'titlebar',
                title: '找回密码',
                items: [
                    {
                        text: '返回',
//                        handler:function(){
//                            var backid = Ext.Viewport.getInnerItems().length - 2;
//                            Ext.Viewport.remove(this.up('findPassword'), true);
//                            Ext.Viewport.setActiveItem(backid);
//                        }
                        handler:gotoFn
                    }
                ]
            },
            {
                xtype:'image',
                src:'resources/images/icon_login.png',
                height:'182px',
                width:'178px',
                margin:'22px auto 6px auto'
            },
            {
                xtype: 'fieldset',
                style:'margin:3px 22px',
                id:'get_valid',
                hidden:false,
                items: [
                    {
                        style:'padding: 10px 9px;color:#fff; font-size:14px;',
                        html:'请输入手机号，获取验证码'
                    },
                    {
                        xtype: 'textfield',
                        name: 'name',
                        id: 'user_tel',
                        style:'margin-top: 30px;',
                        placeHolder: '手机号',
                        autoCapitalize: true,
                        required: true
                    },
                    {
                        xtype: 'button',
                        cls:'backButton',
                        id:'get_valid_button',
                        style:'margin-top: 26px;',
                        text: '获取验证码'
                    }
                ]
            },
            {
                xtype: 'fieldset',
                id:'valid',
                style:'margin:3px 22px',
                hidden:true,
                items: [
                    {
                        style:'padding: 10px 9px;color:#fff; font-size:14px;',
                        html:'请输入验证码，点击确认进行验证'
                    },
                    {
                        xtype: 'textfield',
                        name: 'name',
                        id: 'valid_num',
                        style:'margin-top: 30px;',
                        placeHolder: '输入验证码',
                        autoCapitalize: true,
                        required: true
                    },
                    {
                        xtype: 'button',
                        id:'valid_button',
                        cls:'backButton',
                        style:'margin-top: 26px;',
                        text: '验证'
                    }
                ]
            },
            {
                xtype: 'fieldset',
                id:'modify_password',
                style:'margin:3px 22px',
                hidden:true,
                items: [
                    {
                        style:'padding: 10px 9px;color:#fff;font-size:14px;',
                        html:'请输入新密码，点击提交'
                    },
                    {
                        xtype: 'passwordfield',
                        name: 'name',
                        id:'new_password',
                        style:'margin-top: 15px;',
                        placeHolder: '输入新密码',
                        autoCapitalize: true,
                        required: true
                    },
                    {
                        xtype: 'passwordfield',
                        name: 'name',
                        id:'re_password',
                        placeHolder: '请确认密码',
                        autoCapitalize: true,
                        required: true
                    },
                    {
                        xtype: 'button',
                        cls:'backButton',
                        id:'modify_password_button',
                        text: '提交'
                    }
                ]
            }

        ],
        listeners: {
            initialize: function() {
            },
            painted: function() {
                Ext.Viewport.unmask();
            },
            back:gotoFn
        }
    }

});