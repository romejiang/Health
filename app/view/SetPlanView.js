
/*信息详情*/
Ext.define('Health.view.SetPlanView', {
    extend: 'Ext.Container',
    xtype: 'setplanview',

    requires:[
        'Ext.data.Store',
        'Ext.field.Search',
        "Ext.dataview.List"
    ],
    config:{
        id:"planViewConfig",
        style: 'background:#d6e2e2;',
        scrollable:true,
        items:[
            {
                xtype:'toolbar',
                ui:'dark',
                title:'',
                id:"planName",
                docked:'top',
                items:{
                    xtype:'button',
                    text:'返回',
                    handler:gotoFn
                },
                style:'z-index:2'
            },
            {
                xtype:'panel',
                style: 'background:#d6e2e2;',
                items:[
                    {
                        xtype:'fieldset',
                        flex:1,
                        style: 'padding:20px 0 5px 0',
                        cls:'x-form-fieldset2',
                        items:[
                            {
                                xtype: 'datepickerfield',
                                style: 'background:#fff; ',
                                name: 'takedate',
                                id: 'plan_start_date',
                                dateFormat: 'Y-m-d',
//                                value: new Date(),
                                picker: {
                                    yearFrom: new Date().getFullYear() - 4,
                                    slotOrder: ['year', 'month', 'day'],
                                    doneButton: {text: '完成'},
                                    cancelButton: {text: '取消',
                                    listeners:{
                                        tap:function(){
                                            Ext.getCmp("plan_start_date").setValue();
                                        }
                                    }
                                }
                                },listeners:{
                                    change:function(){
                                        localRemove("keyValue");
                                    },
                                    focus:function(){
                                        var fectype=localGet('fectype');
                                         if(fectype==7){
                                            this.setValue(new Date((new Date().getFullYear()-1)+"-7-15"));
                                        }else if(fectype==8){
                                            var d=new Date();
                                            this.setValue(new Date(d.getFullYear() + "-" + d.getMonth() + "-15"));
                                        }
                                    }
                                    
                                }
                            },
                            {
                                xtype:'button',
                                cls:'backButton',
                                style: 'margin:14px 20px 0 20px',
                                text:'查看全部提醒',
                                action:'planSearch'
                            }
                        ]
                    }
                    ,{
                        xtype:'panel',
                            cls:'feUseDetail',

                        flex:2,
                        html: ""
                    }
                ]
            }

        ]
    }
});

