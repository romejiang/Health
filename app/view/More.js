Ext.define('Health.view.More', {
    extend: 'Ext.Panel',
    xtype: 'morePanel',

    requires: [
        "Ext.data.Store",
        "Ext.dataview.List",
        "Ext.data.proxy.Ajax",
        'Ext.form.FieldSet',
        'Ext.Label'
    ],
    config: {
        title: '更多',
        iconCls: 'mymore ',
        scrollable: false,
        height: '100%',
        layout: {
            type: 'card'
        },
        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                title: '更多',
                style: 'z-index:2',
                items: [
                    {xtype: 'spacer'},
                    {
                        xtype: 'button',
                        text: '账户',
                        handler: function () {
                            gotoFn("account")
                            return false;
                        }
                    }
                ]
            },
            {
                xtype: 'panel',
                style: 'background:#d6e2e2;',
                items: [
                    {
                        xtype: 'fieldset',
                        cls: 'moreFrom',
                        algin: 'center',
                        style: 'padding-top: 10px',
                        html: '<div  class=" moreFrom2 moreFont1" style=" border-radius:8px">' +
                            '<span>我的信息   </span>' +
                            '<div style="float: left;margin-right: 10px;margin-top:-2px;"><img src="resources/images/more_1.png"  height="20px" style=" vertical-align: middle"/></div>' +
                            '<img src="resources/images/go_0.png" style=" height: 20px; float:right; margin-top:16px"/>' +
                            '</div>',
                        listeners: {
                            painted: function (label,opts) {
                                label.on('tap', function () {
                                    var uid = localGet('app_Login_id');
                                    var uname = localGet('app_Login_name');
                                    if (uid == null) {
                                        gotoFn("loginPanel")
                                        return false;
                                    }
                                    gotoFn("myInforPanel")
                                    return false;
                                }, this)
                            }
                        }
                    },
                    {
                        xtype: 'fieldset',
                        cls: 'moreFrom',
                        algin: 'center',
                        items: [
                            {
                                html: '<div class="moreFrom2 moreFont1" style=" border-radius:8px 8px 0px 0px;border-bottom: 0px">' +
                                    '<span>分&nbsp;&nbsp;享&nbsp;&nbsp;至</span>' +
                                    '<div style="float: left;margin-right: 10px;margin-top:-2px;"><img src="resources/images/more_7.png"  height="20px" style=" vertical-align: middle"/></div>' +
                                    '<img src="resources/images/go_0.png" style=" height: 20px; float:right; margin-top:16px"/>' +
                                    '</div>',
                                listeners: {
                                    painted: function (label) {
                                        label.on('tap', function () {
                                            cordova.exec(function (winParam) {
                                                },
                                                function (error) {
                                                },
                                                "Share",
                                                "share",
                                                ["西安社区卫生上线啦 下载地址：http://www.xayoudao.com/health/", "http://www.xayoudao.com/health/logo.png", "http://www.xayoudao.com/health/"]);
                                            return false;
                                        }, this);
                                    }
                                }
                            },
                            {
                                html: '<div class="moreFrom2 moreFont1" style="border-bottom: 0px">' +
                                    '<div style="float: left;margin-right: 10px;margin-top:-2px;"><img src="resources/images/more_3.png"  height="20px" style=" vertical-align: middle"/></div>' +
                                    '<span>版本更新</span>' +
                                    '<img src="resources/images/go_0.png" style=" height: 20px; float:right; margin-top:16px"/>' +
                                    '</div>',
                                listeners: {
                                    painted: function (label) {
                                        label.on('tap', function () {
                                            if (Ext.os.is.Android) {
                                                cordova.exec(function (message) {
                                                    },
                                                    function (error) {
                                                    },
                                                    "CheckVersion",
                                                    "checkVersion",
                                                    ['']
                                                );
                                                return false;
                                            } else {
                                                cordova.exec(function (message) {
                                                    },
                                                    function (error) {
                                                    },
                                                    "CheckVersion",
                                                    "checkVersion",
                                                    ['698029105']
                                                );
                                                return false;
                                            }

                                        }, this);
                                    }
                                }
                            },
                            {
                                html: '<div class="moreFrom2 moreFont1" style=" border-radius:0px 0px 8px 8px">' +
                                    '<div style="float: left;margin-right: 10px;margin-top:-2px;"><img src="resources/images/more_2.png"  height="20px" style=" vertical-align: middle"/></div>' +
                                    '<span>关&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;于</span>' +
                                    '<img src="resources/images/go_0.png" style=" height: 20px; float:right; margin-top:16px"/>' +
                                    '</div>',
                                listeners: {
                                    painted: function (label) {
                                        label.on('tap', function () {
                                            gotoFn("moreInfor");
                                            return false;
                                        }, this);
                                    }
                                }
                            }
                        ]
                    }
                ]
            }
        ]
    }
});



