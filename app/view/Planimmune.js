/**
 * Created with JetBrains WebStorm.
 * User: Administrator
 * Date: 13-7-9
 * Time: 下午3:18
 * To change this template use File | Settings | File Templates.
 */
/*信息列表*/
Ext.define('Health.view.Planimmune', {
    extend: 'Ext.Panel',
    xtype: 'planimmune',
    count:0,
    requires: [
        'Ext.data.Store',
        'Ext.field.Search',
        "Ext.dataview.List"
    ],
    config: {
        layout: 'card',
        style: 'background:transparent;',
        scrollable:true,
        items: [
            {
                xtype: 'toolbar',
                ui: 'dark',
                docked: 'top',
                title: '计划免疫',
                style: 'z-index:1',
                items: {
                    xtype: 'button',
                    text: '返回',
                    handler: gotoFn
                }
            },
            {
                xtype:'panel',
                layout:'vbox',
                style:'padding:10px; font-size:0.9em',
                items:
                [
                    {
                        xtype: 'panel',
                        style:'background:#fff;border-radius: 8px;line-height:2.5em;',
                        algin: 'center',
                        items:[
                            {
                                style:'color:#146d6d;border-bottom: 1px solid #dbdfde;',
                                html: '<div style="text-align:center;">下次接种的疫苗的名称和剂次</div>'
                            },
                            {
                                id:'immunityDetail'
                            }
                        ]
                    },
                    {
                        xtype:'button',
                        cls:'shoot0',
                        id:'shooted'
                    },
                    {
                        xtype: 'panel',
                        style:'background:#fff;border-radius: 8px;line-height:2.5em;',
                        algin: 'center',
                        items:[
                            {
                                style:'color:#146d6d;border-bottom: 1px solid #dbdfde;',
                                html: '<div style="text-align:center;">下次接种时间</div>'
                            },
                            {
                                style: '-webkit-box-orient: horizontal; border:none;text-align:center;margin: 0px auto;width: 110px;',
                                id:'putOffDate',
                                xtype: 'datepickerfield',
                                dateFormat: 'Y-m-d',
                                value:new Date(),
                                picker: {
                                    yearFrom: '2000',
                                    slotOrder: ['year', 'month', 'day'],
                                    doneButton: {text: '完成',id:'putOffDate_done'},
                                    cancelButton: {text: '取消'}
                                }
                            }
                        ]
                    },
                    {
                        xtype:'button',
                        id:'putOffBtn',
                        cls:'changeTime1',
                        listeners:{
                            tap:function(){
                                Ext.getCmp('putOffDate').focus();
                            }
                        }
                    },
                    {
                        xtype:'panel',
                        cls:'immunityIntroduce',
                        id:'immunityIntroduce'
                    }
                ]
            }
        ], listeners: {
            back: gotoFn
        }

    }

});

