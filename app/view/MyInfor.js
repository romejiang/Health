Ext.define('Health.view.MyInfor', {
    extend:'Ext.form.Panel',
    xtype:'myInforPanel',

    requires:[
        'Ext.data.Store',
        'Ext.form.FieldSet',
        'Ext.field.Toggle'
    ],

    config:
    {
        layout:'card',
        scrollable:false,
        height:'100%',
        items:
            [
                {
                    xtype:'toolbar',
                    docked:'top',
                    title:'我的信息',
                    items:{
                        xtype:'button',
                        text:'返回',
                        handler:gotoFn
                    }
                },
                {
                    xtype: 'fieldset',
                    title: '个人基本信息',
                    style:'margin:12px;background:#f4f4f4',
                    scrollable:true,
                    items:
                        [
                            {
                                xtype: 'textfield',
                                name:'name',
                                label: '姓名',
                                id:'username'
                            },
                            {
                                xtype: 'togglefield',
                                name: 'sex',
                                label: '性别',
                                id:'usersex',
                                style:'position:relative',
                                html:'<div style="position:absolute;top:10px;left:15px;font-size:14px;color:#818588 ">男</div><div style="position:absolute;top:10px;left:112px;font-size:14px;color:#818588">女</div>'
                            },
                            {
                                xtype: 'datepickerfield',
                                style: 'background:#fff; ',
                                id: 'userbirth',
                                label: '出生日期:',
                                dateFormat: 'Y-m-d',
                                value: new Date(),
                                picker: {
                                    yearFrom: '1900',
                                    yearTo: (new Date().getFullYear()),
                                    slotOrder: ['year', 'month', 'day'],
                                    doneButton: {text: '完成'},
                                    cancelButton: {text: '取消'}
                                }
                            },
                            {
                                xtype: 'textfield',
                                id:'usertel',
                                name: 'mobile_num',
                                label: '电话',
                                readOnly:'true'
                            },
                            {
                                xtype: 'textareafield',
                                name: 'address',
                                id:'useraddress',
                                label: '住址',
                                height:'100px'
                            },
                            {
                                xtype:'panel',
                                layout:'hbox',
                                style:'background:#f4f4f4;padding-top:6px;',
                                items:[
                                    {
                                        xtype:'button',
                                        cls:'form_confirm',
                                        width:'65%',
                                        id:'myinfor_confirm',
                                        style:'margin:0 2% 0 0;height:45px;'

                                    },
                                    {
                                        xtype:'button',
                                        cls:'form_cancle',
                                        width:'31%',
                                        style:'margin:0 2% 0 0;height:45px;',
//                                        handler:function(){
////                                            var backid = Ext.Viewport.getInnerItems().length - 2;
//                                            Ext.Viewport.remove(this.up('myInforPanel'), true);
////                                            Ext.Viewport.setActiveItem(backid);
//                                        }
                                        handler:gotoFn
                                    }
                                ]
                            }
                        ]
                }

            ],
        listeners: {
            painted:function(){
                var uid = localGet('app_Login_id');
                console.log("uid==="+uid)
                var uname = localGet('app_Login_name');
                Ext.Ajax.request({
                    url: Global.api_url + '/cloud/1/users_info_get',
                    params: {cuid: uid},
                    scope: this,
                    success: function (response) {
                        var result = Ext.decode(response.responseText);
                        var user = result.Variables.data;
                        Ext.getCmp('username').setValue(user.name);
                        Ext.getCmp('useraddress').setValue(user.address);
                        Ext.getCmp('usertel').setValue(user.mobile_num);
                        Ext.getCmp('usersex').setValue(user.sex);
                        if(user.birthyear!=0){
                            var birth=user.birthyear+'-'+user.birthmonth+'-'+user.birthday;
                            Ext.getCmp('userbirth').setValue(new Date(birth));
                        }

                    },
                    failure: function () {
                        cordova.exec(function (message) {
                        }, function (error) {
                        }, "Messages", "showMsg", ['请求失败', '服务器维护中....']);
                        return false;
                    }
                })
            },
            back: function () {
                Ext.Viewport.getActiveItem().pop();
            }
        }
    }
});

