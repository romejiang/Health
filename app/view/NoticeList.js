/*信息列表*/
Ext.define('Health.view.NoticeList', {
    extend: 'Ext.Panel',
    xtype: 'notice',

    requires:[
        'Ext.data.Store',
        'Ext.field.Search',
        "Ext.dataview.List",
        "Ext.plugin.PullRefresh",
        "Ext.plugin.ListPaging"
    ],

    config:{
        layout:'card',
        style: 'background:#d6e2e2',
        items:[
            {
                xtype:'toolbar',
                ui: 'dark',
                docked:'top',
                title:'最新通知',
                items:{
                    xtype:'button',
                    text:'返回',
                    handler:gotoFn
                },
                style:'z-index:2'
            },
            {
                xtype: 'list',
                store: 'Notice',
                style: 'background:#d6e2e2',
                pressedCls:false,
                selectedCls:false,
                cls:'x-list5',
                emptyText:'<div style="text-align: center;width: 100%;">暂无内容</div>',
                itemTpl: [
                    '<div style="background-color: #c3c3c3; border-radius: 8px; padding: 8px; margin:0 4px;">' +
                        '<div style="font-size: 14px; line-height: 22px;">{noticecontent}</div>' +
                        '<div style="font-size: 12px; line-height: 22px; text-align: right">{[Ext.Date.format(new Date(parseInt(values.createtime) * 1000), "Y-m-d H:i")]}</div>' +
                        '</div>'
                ].join(''),
                listeners: {
                    initialize: function () {
                        var notice = Ext.getStore('Notice');
                        notice.getProxy().setExtraParam('cadid', localStorage.getItem('cadid'));
                        notice.loadPage(1);
                    }
                },
                plugins: [
                    {
                        xclass: 'Ext.plugin.PullRefresh',
                        pullRefreshText: '下拉可以更新',
                        releaseRefreshText: '松开开始更新',
                        style: 'top:12px',
                        loadingText: '正在刷新……',
                        refreshFn: function (loaded, arguments) {
                             loaded.getList().getStore().loadPage(1, {
                                callback: function (record, operation, success) {
                                    Ext.Viewport.unmask();
                                }, scope: this });
                        }
                    },
                    {
                        xclass: 'Ext.plugin.ListPaging',
                        loadMoreText: '<h2 style="text-align: center; padding-top: 20px; padding-bottom: 20px;">点击加载更多。。。。</h2>',
                        noMoreRecordsText: '<h2 style="text-align: center; padding-top: 20px; padding-bottom: 20px;">没有更多条记录了</h2>',
                        autoPaging: false //设置为TRUE将自动触发
                    }
                ]
            }
        ],listeners: {
            back: gotoFn
        }

    }

});

