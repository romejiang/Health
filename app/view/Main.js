Ext.define('Health.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'mainPanel',
    requires: [],
    config: {
        tabBarPosition: 'bottom',
        params:{

        },
        items: [
            {
                xtype:'index',
                title:'首页',
                iconCls:'home'
            },
            {
                xtype:'homePanel',
                title:'菜单',
                iconCls:'list'
            },
            {
                xtype:'monitorPanel',
                title:'我的监测',
                iconCls:'chart2'
            },
            {
                xtype:'adviceCategory',
                title:'健康交流',
                iconCls:'chat2'
            },
            {
                xtype:'morePanel',
                title:'更多',
                iconCls:'more'
            }
        ]
    }
});
