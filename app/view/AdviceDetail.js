/*咨询详细信息*/
Ext.define('Health.view.AdviceDetail', {
    extend: 'Ext.Panel',
    xtype: 'adviceDetail',

    requires:[
        'Ext.data.Store',
        'Ext.field.Search'
    ],


    config:{
        layout:'vbox',
    //    scrollable:true,
    //    style: 'background:#d6e2e2;',
        items:[
            {
                xtype:'toolbar',
                docked:'bottom',
                id:'sendID',
                items:[
                    {
                        xtype:'textfield',
                        id:'textsend',
                        width:'80%'
                    },
                    {
                        xtype:'button',
                        text:'发送',
                        action:'send'
                    }
                ]
            },
            {
                id:'consultid',
                //html:'<input type="hidden" value="" id="consultid"/>',
                hidden:true
            },
            {
                xtype:'titlebar',
                docked:'top',
//                title:'专家咨询',
                items:{
                    xtype:'button',
                    text:'返回',
//                    handler:function(){
//                        var backid = Ext.Viewport.getInnerItems().length - 2;
//                        Ext.Viewport.remove(this.up('adviceDetail'), true);
//                        Ext.Viewport.setActiveItem(backid);
//                    }
                    handler:gotoFn
                }
            },
            {
                xtype:'dataview',
                id:'commentList',
                store:'AdviceDetail',
                style: 'background:#d6e2e2;',
                emptyText:'<div style="text-align: center; position: absolute; bottom: 10px; width: 100%; font-size: 14px; color: #676767">暂无回复</div>',
                scrollable: true,
                height:'100%',
                html:'asdfasdf',
                itemTpl : new Ext.XTemplate(
                    '<tpl if="status==1">',
                    '   <div style="min-height:50px; margin:10px 10px  10px 10px" >',
                    '       <div class="nick" style="float:left;"><img src="resources/images/sick.png" height="40" style="border-radius: 8px; padding: 2px 0px 0px 2px;"/></div>',
                    '       <div class="chatbox title">',
                    '           <p class="message" style="word-wrap:break-word;white-space:normal">{messages}</p>',
                    '           <p class=" message" style="white-space:normal;word-wrap:break-word; ">',
                    '               <span style="font-size: 12px;color: #999;line-height: 16px;">用户:</span>',
                    '               <span style="font-size: 12px;color: #999;line-height: 16px;">{uname}&nbsp;&nbsp;</span>',
                    '               <span style="font-size: 10px;color: #999;line-height: 16px; margin: 5px 0 0 0px">{[Ext.Date.format(new Date(parseInt(values.lasttime) * 1000), "Y-m-d")]}</span>',
                    '           </p>',
                    '       </div>',
                    '   </div>',
                    '<tpl else>',
                    '   <div style="min-height:50px; margin:10px 10px  10px 10px;">',
                    '       <div class="nick" style="float:right; height:45px; width:45px; word-wrap:break-word;"><img src="resources/images/sick.png" height="35" width="35" style="border-radius: 8px; padding: 2px 0px 0px 2px;"/></div>',
                    '       <div class="chatbox content" >',
                    '           <p class="message" style="word-wrap:break-word;white-space:normal">{messages}</p>',
                    '           <p class=" message" style="white-space:normal;word-wrap:break-word;">',
                    '               <span style="font-size: 12px;color: #999;line-height: 16px;">用户:</span>',
                    '               <span style="font-size: 12px;color: #999;line-height: 16px;">{uname}&nbsp;&nbsp;</span>',
                    '               <span style="font-size: 10px;color: #999;line-height: 16px; margin: 5px 0 0 0px">{[Ext.Date.format(new Date(parseInt(values.lasttime) * 1000), "Y-m-d")]}</span>',
                    '           </p>',
                    '       </div>',
                    '   </div>',
                    '</tpl>'
                )

            }
        ],
        listeners:{
            back:gotoFn
        }
    }
});





