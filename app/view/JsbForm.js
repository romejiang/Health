Ext.define('Health.view.JsbForm', {
	extend:'Ext.Panel',
	xtype:'jsbFormPanel',

	requires:[
        'Ext.data.Store',
        'Ext.form.FieldSet',
        'Ext.field.Toggle'
    ],

    config:
    {
        layout:'card',
        items:
        [
            {
                xtype:'toolbar',
                docked:'top',
                title:'我的监测',
                items:{
                    xtype:'button',
                    text:'返回',
                    handler:function(){
                        gotoFn("jsbListPanel");
                        return false;
                    }
                }
            },
            {
                xtype: 'fieldset',
                title: '重症精神病患者登记',
                style:'margin:12px;background:#f4f4f4',
                scrollable:true,
                items:
                [
                   {
                       xtype: 'textfield',
                       name:'name',
                       label: '姓名',
                       id:'name'
                   },
                   {
                       xtype: 'textfield',
                       name: 'age',
                       label: '年龄',
                       id:'age'
                   },
                   {
                       xtype: 'textfield',
                       name: 'jibing',
                       label: '疾病名称',
                       id:'jibing'
                   },
                   {
                       xtype: 'textfield',
                       name: 'tel',
                       label: '联系电话',
                       id:'tel'
                   },
                   {
                       xtype: 'textfield',
                       name: 'jianhuren',
                       label: '监护人',
                       id:'jianhuren'
                   },
                    {
                        xtype:'panel',
                        layout:'hbox',
                        style:'background:#f4f4f4',
                        items:[
                            {
                                xtype:'button',
                                cls:'form_confirm',
//                                text:'提交',
                                action:'jsbFormSubmit',
                                width:'65%',
                                style:'margin:0 2% 0 0;height:45px;'

                            },
                            {
                                xtype:'button',
//                                text:'取消',
                                cls:'form_cancle',
                                action:'jsbformCancel',
                                width:'31%',
                                style:'margin:0 2% 0 0;height:45px;'
                            }
                        ]
                    }
                ]
            }

        ],
        listeners: {
            back: function () {
                Ext.Viewport.getActiveItem().pop();
            }
        }
    }

});


 