Ext.define('Health.view.Ask', {
    extend: 'Ext.Panel',
    xtype: 'askPanel',

    requires: [
        'Ext.data.Store',
        'Ext.form.FieldSet',
        'Ext.field.Toggle'
    ],
    config: {
        title: '专家咨询',
        style: 'background:#d6e2e2;',
        scrollable: true,
        height: '100%',
        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                title: '专家咨询',
                items: {
                    xtype: 'button',
                    text: '返回',
                    handler: function () {
                        if (Ext.getStore('Advice').getCount() < 1) {
                            Ext.getStore('Advice').load({params: {uid: localGet('app_Login_id')}});
                            Ext.getStore('Advice').filter('cid', localGet('cid'));
                        } else {
                            Ext.getStore('Advice').filter('cid', localGet('cid'));
                        }
                        gotoFn()
                    }
                }

            },
            {
                xtype: 'fieldset',
                style: 'margin:12px 0 0 0',
                items: [
                    {
                        xtype: 'textareafield',
                        id: 'askText',
                        placeHolder: '输入问题',
                        style: 'height:140px;'
                    },
                    {
                        xtype: 'togglefield',
                        name: 'disabled',
                        id: 'consult_disabled',
                        label: '是否公开',
                        labelWidth: '30%',
                        value: '1'
                    },
                    {
                        layout: 'hbox',
                        style: 'margin:20px 0px 12px 0px',
                        items: [
                            {
                                xtype: 'button',
                                cls: 'ask_confirm',
                                width: '65%',
                                action: 'asksubmit',
                                style: 'margin:0 2% 0 0;'

                            },
                            {
                                xtype: 'button',
                                width: '31%',
                                cls: 'ask_cancle',
                                style: 'margin:0 0 0 2%;',
                                handler: function () {
                                    Ext.getCmp('askText').reset();
                                    gotoFn();
                                }
                            }
                        ]
                    }
                ]
            }


        ],
        listeners: {
//            back: function () {
//                var backid = Ext.Viewport.getInnerItems().length - 2;
//                Ext.Viewport.remove(Ext.Viewport.getActiveItem(), true);
//                Ext.Viewport.setActiveItem(backid);
//            }
            back: gotoFn
        }
    }

});


 