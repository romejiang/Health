/*
 This file is generated and updated by Sencha Cmd. You can edit this file as
 needed for your application, but these edits will have to be merged by
 Sencha Cmd when it performs code generation tasks such as generating new
 models, controllers or views and when running "sencha app upgrade".

 Ideally changes to this file would be limited and most work would be done
 in other places (such as Controllers). If Sencha Cmd cannot merge your
 changes and its generated code, it will produce a "merge conflict" that you
 will need to resolve manually.
 */

// DO NOT DELETE - this directive is required for Sencha Cmd packages to work.
//@require @packageOverrides

//<debug>
Ext.Loader.setPath({
    'Ext': 'touch/src',
    'Health': 'app'
});
//</debug>

Ext.application({

    name: 'Health',

    requires: [
        'Ext.MessageBox'
    ],

    controllers: ['Main', 'Monitor', 'Advice', 'Login', 'Register', 'Area', 'MyInfor', 'FindPassword'],
    views: ['Main', 'Home', 'Register', 'Login', 'InforList', 'InforDetail', 'Area', 'MyInfor', 'PopMap',
        'Advice', 'AdviceCategory', 'Ask', 'AdviceDetail',
        'Monitor', 'TnbForm', 'GxyForm', 'JsbForm', 'TnbList',
        'GxyList', 'JsbList', 'OrderDate', 'SB','NoticeList',
        'BaiduMap', 'More', 'Medicare', 'Immunity', 'Planimmune', 'Order', 'Index', 'MoreInfor', 'FindPassword', 'Account',"SetPlanView","PlanList"],

    stores: ['Area', 'Home', 'Monitor', 'Advice', 'Tnb', 'Gxy', 'Infor', 'AdviceCategory', 'AdviceDetail', 'Immunity', 'Order', 'HealAdpic', 'OrderDate','Notice'],
    models: ['Area', 'Home', 'Monitor', 'Advice', 'Tnb', 'Gxy', 'Infor', 'AdviceCategory', 'AdviceDetail', 'Immunity', 'Order', 'HealAdpic', 'OrderDate','Notice'],


    icon: {
        '57': 'resources/icons/Icon.png',
        '72': 'resources/icons/Icon~ipad.png',
        '114': 'resources/icons/Icon@2x.png',
        '144': 'resources/icons/Icon~ipad@2x.png'
    },

    isIconPrecomposed: true,

    startupImage: {
        '320x460': 'resources/startup/320x460.jpg',
        '640x920': 'resources/startup/640x920.png',
        '768x1004': 'resources/startup/768x1004.png',
        '748x1024': 'resources/startup/748x1024.png',
        '1536x2008': 'resources/startup/1536x2008.png',
        '1496x2048': 'resources/startup/1496x2048.png'
    },

    launch: function () {
        // Ext.Viewport.add(Ext.create('Health.view.Home'));
        // return;
        // Destroy the #appLoadingIndicator element
        Ext.fly('appLoadingIndicator').destroy(); 
        // Initialize the main view
        var store = Ext.getStore('Area');
        store.load({params: {caid: 1}});
        cordova.exec(function (message) {
            localStorage.setItem('udid', message);
        }, function (error) {
        }, "GetUUID", "getUUID", [""]); 
        if (window.localStorage.getItem('cadid') == null) {
            Ext.Viewport.add(Ext.create('Health.view.Area'));
        } else {
            Ext.Viewport.add(Ext.create('Health.view.Main'));
        }
//       Ext.Viewport.add(Ext.create('Health.view.Main'));
        document.addEventListener('deviceready', function() {
            // 页面加载完成时，关闭 splash 界面
            var times=new Date().getTime();
            navigator.splashscreen.hide();
            //当应用启动的时候，监听backbutton的事件
            if (Ext.os.is.Android) {
                document.addEventListener("backbutton", function() {
                    if ((Ext.Viewport.getActiveItem().id).indexOf("main") != -1) {
                        cordova.exec(function (message) { }, function (error) {
                        }, "Messages", "showMsg", ['再按一次退出西安社区卫生']);
                        if((new Date().getTime()- times) < 2000){
                            navigator.app.exitApp();
                        }  else {
                            times = new Date().getTime();
                        }
                    } else {
                        Ext.Viewport.getActiveItem().fireEvent('back');
                    }
                }, false);
            }
        }, false);

    },

    onUpdated: function () {
        Ext.Msg.confirm(
            "Application Update",
            "This application has just successfully been updated to the latest version. Reload now?",
            function (buttonId) {
                if (buttonId === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
