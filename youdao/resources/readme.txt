图标，共四种规格
./icons:
icon-72.png   72*72
icon-72@2x.png   144*144
icon.png         57*57
icon@2x.png      114*114

欢迎界面
./splash:
Default-568h@2x~iphone.png     640*1136
Default@2x~iphone.png          640*960
Default~iphone.png             320*480

帮助页面的图片
./help:
1@2x~iphone.png         640*960
1~iphone.png            320*480
2@2x~iphone.png			640*960
2~iphone.png            320*480
3@2x~iphone.png			640*960
3~iphone.png     		320*480


跳过按钮的图片
./help/skip/highlighted:
skip,x=10,y=10,width=10,height=10@2x~iphone.png

./help/skip/normal:
skip,x=10,y=10,width=40,height=16@2x~iphone.png



Default-568h@2x~iphone.png (640x1136 pixels)
Default-Landscape@2x~ipad.png (2048x1496 pixels)
Default-Landscape~ipad.png (1024x748 pixels)
Default-Portrait@2x~ipad.png (1536x2008 pixels)
Default-Portrait~ipad.png (768x1004 pixels)
Default@2x~iphone.png (640x960 pixels)
Default~iphone.png (320x480 pixels)